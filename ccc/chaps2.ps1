<#
chaps2.ps1 - a PowerShell script for checking system security
            when conducting an assessment of systems where
            the Microsoft Policy Analyzer and other assessment
            tools cannot be installed.
#>

<#
License: 
Copyright (c) 2019, Cutaway Security, Inc. <don@cutawaysecurity.com>
modified vPierre (c) 2020
 
chaps2.ps1 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
 
chaps.ps1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Point Of Contact:    Don C. Weber <don@cutawaysecurity.com>
#>

<#
The best way to run this script within an ICS environment is to not write any programs or scripts to the system being reviewed. Do this by serving these scripts from a webserver running on another system on the network. Download CHAPS and PowerSploit into the same directory and open a terminal and change into that directory. Using Python3 run the command 'python3 -m http.server 8181'. This will start a webserver listening on all of the systems IP addresses. 

On the target system open a CMD.exe window, preferably as an Administrator. Run the command `powershell.exe -exec bypass` to being a PowerShell prompt. From this prompt, run the following command to execute the `chaps.ps1` script.

```
IEX (New-Object Net.WebClient).DownloadString('http://<webserver>:8181/chaps/chaps.ps1')
```

To run the `chaps-powershell.ps1` script be sure to turn off the system's Anti-virus to include real-time protection. Running the following commands will import the appropriate PowerSploit scripts and then run them.

```
IEX (New-Object Net.WebClient).DownloadString('http://<webserver>:8181/PowerSploit/Recon/PowerView.ps1')
IEX (New-Object Net.WebClient).DownloadString('http://<webserver>:8181/PowerSploit/Exfiltration/Get-GPPPassword.ps1')
IEX (New-Object Net.WebClient).DownloadString('http://<webserver>:8181/PowerSploit/Exfiltration/Get-GPPAutologon.ps1')
IEX (New-Object Net.WebClient).DownloadString('http://<webserver>:8181/PowerSploit/Exfiltration/Get-VaultCredential.ps1')
IEX (New-Object Net.WebClient).DownloadString('http://<webserver>:8181/PowerSploit/Privesc/PowerUp.ps1')
IEX (New-Object Net.WebClient).DownloadString('http://<webserver>:8181/chaps/chaps-powersploit.ps1')
```

Script outputs will be written to the user's Temp directory as defined by the $env:emp variable.

Useful Resources:
    New tool: Policy Analyzer: https://blogs.technet.microsoft.com/secguide/2016/01/22/new-tool-policy-analyzer/
    Use PowerShell to Explore Active Directory Security: https://blogs.technet.microsoft.com/heyscriptingguy/2012/03/12/use-powershell-to-explore-active-directory-security/
    Penetration Testers’ Guide to Windows 10 Privacy & Security: https://hackernoon.com/the-2017-pentester-guide-to-windows-10-privacy-security-cf734c510b8d
    15 Ways to Bypass the PowerShell Execution Policy: https://blog.netspi.com/15-ways-to-bypass-the-powershell-execution-policy/
#>

########## Create storage directory for files in users Temp directory at $env:temp ##############
$chaps_dest = "chaps-$(get-date -f yyyyMMdd-hhmmss)"
New-Item -ItemType directory -Path $env:temp\$chaps_dest
$out_file = "c:\log\ComputerName-chaps.txt"
$sysinfo_file = "$env:temp\$chaps_dest\$Env:Computername-sysinfo.txt"
###############################

########## Output Header Write-Host Functions ##############
# Postive Outcomes - configurations / settings that are, at a minimum, expected.
$pos_str = "[+] "
$neg_str = "[-] "
$inf_str = "[*] "
$rep_str = "[$] "
$err_str = "[x] "
###############################

########## Check for Administrator Role ##############
$inf_str + "Start Date/Time: $(get-date -format yyyyMMddTHHmmssffzz)" | Tee-Object -FilePath $out_file -Append
if (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")){ 
    $neg_str + "You do not have Administrator rights. Some checks will not succeed. Note warnings." | Tee-Object -FilePath $out_file -Append
} else {
    $inf_str +  "Script running with Administrator rights." | Tee-Object -FilePath $out_file -Append
}

# Suppress All Errors? Uncomment this line
# Don't use this, we need to manually handle each error to produce Write-Err
#$ErrorActionPreference = "SilentlyContinue"
###############################

########### Gather System Info #############
$inf_str +  "Dumping System Info to seperate file\n" | Tee-Object -FilePath $out_file -Append
systeminfo  | Tee-Object -FilePath $sysinfo_file -Append
###############################

########## Check for Windows Information ##############
$inf_str + "Windows Version: $(([Environment]::OSVersion).VersionString)" | Tee-Object -FilePath $out_file -Append
$inf_str + "Windows Default Path for $env:Username : $env:Path" | Tee-Object -FilePath $out_file -Append

$inf_str + "Checking IPv4 Network Settings"
Try{
    $ips = (Get-NetIPAddress | Where AddressFamily -eq 'IPv4' | Where IPAddress -ne '127.0.0.1').IPAddress
    if ($ips -ne $null){
        foreach ($ip in $ips){
            if ($ip -ne $null){
                #$inf_str + "Host network interface assigned:" $ip
                $inf_str + "Host network interface assigned: $ip" | Tee-Object -FilePath $out_file -Append
            }
        }
    }else{
        # Use Throw function to call the Catch function
        Throw('Get-NetIPAddress error') | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    Try{
        $ips = (gwmi win32_networkadapterconfiguration -filter 'ipenabled=true')
        if ($ips -ne $null){
            foreach ($ip in $ips){
                if ($ip -ne $null){
                    foreach ($i in $ip.IPAddress){
                        if ($i -notmatch ":"){
                            $inf_str + "Host network interface assigned (gwmi): $i" | Tee-Object -FilePath $out_file -Append
                        }
                    }
                }
            }
        } else {
            # Use Throw function to call the Catch function
            Throw('gwmi win32_networkadapterconfiguration error') | Tee-Object -FilePath $out_file -Append
        }
    }
    Catch{
        $err_str + "Check IPv4 Network Settings failed." | Tee-Object -FilePath $out_file -Append
    }
}

$inf_str + "Checking IPv6 Network Settings" | Tee-Object -FilePath $out_file -Append
Try{
    # Checking using gwmi tells us if IPv6 is enabled on an interface because
    # it returns an IPv6 address. Get-NetIPAddress does to, but this is easier.
    $noipv6 = $true
    $ips = (gwmi win32_networkadapterconfiguration -filter 'ipenabled=true')
    if ($ips -ne $null){
        foreach ($ip in $ips){
            if ($ip -ne $null){
                foreach ($i in $ip.IPAddress){
                    if ($i -match ":"){
                        $neg_str + "Host IPv6 network interface assigned (gwmi): $i" | Tee-Object -FilePath $out_file -Append
                        $noipv6 = $false
                    }
                }
            }
        }
        if ($noipv6){
            $pos_str + "No interfaces with IPv6 network interface assigned (gwmi)." | Tee-Object -FilePath $out_file -Append
        }
    } else {
        # Use Throw function to call the Catch function
        Throw('gwmi win32_networkadapterconfiguration error') | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Check IPv6 Network Settings failed (gwmi)." | Tee-Object -FilePath $out_file -Append
}

<#
Resources:
    Managing Windows Update with PowerShell: https://blogs.technet.microsoft.com/jamesone/2009/01/27/managing-windows-update-with-powershell/
#>

$inf_str + "Checking Windows AutoUpdate Configuration" | Tee-Object -FilePath $out_file -Append
# Check Auto Update Configuration
$AutoUpdateNotificationLevels= @{0="Not configured"; 1="Disabled" ; 2="Notify before download"; 3="Notify before installation"; 4="Scheduled installation"}
Try{
    $resa = ((New-Object -com "Microsoft.Update.AutoUpdate").Settings).NotificationLevel
    if ( $resa -eq 4){
        $pos_str + "Windows AutoUpdate is set to $resa : $AutoUpdateNotificationLevels.$resa" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "Windows AutoUpdate is not configuration to automatically install updates: $resa : $AutoUpdateNotificationLevels.$resa" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Windows AutoUpdate test failed." | Tee-Object -FilePath $out_file -Append
}

# List KB IDs of critical and important updates that have not been applied.
<#
Gist Grimthorr/pending-updates.ps1: https://gist.github.com/Grimthorr/44727ea8cf5d3df11cf7
#>
$inf_str + "Checking for missing Windows patches with Critical or Important MsrcSeverity values. NOTE: This make take a few minutes." | Tee-Object -FilePath $out_file -Append
Try{
    $UpdateSession = New-Object -ComObject Microsoft.Update.Session
    $UpdateSearcher = $UpdateSession.CreateupdateSearcher()
    $Updates = @($UpdateSearcher.Search("IsHidden=0 and IsInstalled=0").Updates)
    #$Updates | Select-Object Title
    $missing_updates = $Updates | Where-Object {$_.MsrcSeverity -gt 2} | Select-Object @{Name="KB"; Expression={$_.KBArticleIDs}} |Select-Object -ExpandProperty KB | Sort-Object -Unique 

    if ($missing_updates) {
        foreach ($m in $missing_updates){
            $neg_str + "Missing Critical or Important Update KB: $m" | Tee-Object -FilePath $out_file -Append
        }
    } else {
        $pos_str + "Windows system appears to be up-to-date for Critical and Important patches." | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Check for Critical and Important Windows patches test failed. Review Internet or patch server connectivity." | Tee-Object -FilePath $out_file -Append
}

# Check for BitLocker Disk Encryption
$inf_str + "Checking BitLocker Encryption" | Tee-Object -FilePath $out_file -Append

Try{
    $vs = Get-BitLockerVolume -ErrorAction Stop | Where-Object {$_.VolumeType -eq 'OperatingSystem'} | Select VolumeStatus -ErrorAction Stop
	$resvs = $vs.VolumeStatus 
    if ($resvs -eq 'FullyEncrypted'){
        $pos_str + "BitLocker detected and Operating System Volume is: $resvs" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "BitLocker not detected on Operating System Volume or encryption is not complete. Please check for other encryption methods: $resvs" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
	$vsm = manage-bde -status | Select-String -Pattern 'Conversion Status'
	if ($vsm -ne $null){
	    $resvsm = Select-String -Pattern 'Fully Encrypted'
		if ($resvsm -ne $null){
			$pos_str + "Operating System Volume is Fully Encrypted (manage-bde): $resvsm" | Tee-Object -FilePath $out_file -Append
		} else {
	    	$pos_str + "BitLocker not detected or encryption is not complete. Please check for other encryption methods (manage-bde): $resvsm" | Tee-Object -FilePath $out_file -Append
	    }
	} else {
		$inf_str + "BitLocker not detected. Please check for other encryption methods." | Tee-Object -FilePath $out_file -Append
	}
}

# Check AlwaysInstallElevated Registry Keys
# BugFix 20190523: Mike Saunders - RedSiege, LLC
<#
Resources:
Windows Privilege Escalation Fundamentals: http://www.fuzzysecurity.com/tutorials/16.html
#>
$inf_str + "Checking if users can install software as NT AUTHORITY\SYSTEM" | Tee-Object -FilePath $out_file -Append
Try{
    Try{
        $ressysele = Get-ItemProperty -path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Installer\AlwaysInstallElevated' -ErrorAction stop
    }
    Catch{
        $ressysele = $null;
    }
    Try{
        $resusrele = Get-ItemProperty -path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\Installer\AlwaysInstallElevated' -ErrorAction stop
    }
    Catch{
        $resusrele = $null;
    }

    if ($ressysele -or $resusrele){
            $neg_str + "Users can install software as NT AUTHORITY\SYSTEM." | Tee-Object -FilePath $out_file -Append
    } else {
            $pos_str + "Users cannot install software as NT AUTHORITY\SYSTEM." | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Check if users can install software as NT AUTHORITY\SYSTEM failed" | Tee-Object -FilePath $out_file -Append
}

###############################

########## PowerShell Log Settings ##############
<#
Resources:
    From PowerShell to P0W3rH3LL – Auditing PowerShell: https://www.eventsentry.com/blog/2018/01/powershell-p0wrh11-securing-powershell.html
    Practical PowerShell Security: Enable Auditing and Logging with DSC: https://blogs.technet.microsoft.com/ashleymcglone/2017/03/29/practical-powershell-security-enable-auditing-and-logging-with-dsc/
    PowerShell ♥ the Blue Team: https://blogs.msdn.microsoft.com/powershell/2015/06/09/powershell-the-blue-team/
    PowerShell Security Best Practices: https://www.digitalshadows.com/blog-and-research/powershell-security-best-practices/
    Detecting Offensive PowerShell Attack Tools: https://adsecurity.org/?p=2604
    Windows 10 Protected Event Logging: https://www.petri.com/windows-10-protected-event-logging
    WINDOWS POWERSHELL LOGGING CHEAT SHEET - Win 7/Win 2008 or later: https://static1.squarespace.com/static/552092d5e4b0661088167e5c/t/5760096ecf80a129e0b17634/1465911664070/Windows+PowerShell+Logging+Cheat+Sheet+ver+June+2016+v2.pdf0
#>

$inf_str + "Testing if PowerShell Commandline Audting is Enabled" | Tee-Object -FilePath $out_file -Append
Try{
    if ([bool](Get-ItemProperty -path 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System\Audit' -ErrorAction SilentlyContinue).ProcessCreationIncludeCmdLine_Enabled){
        $pos_str + "ProcessCreationIncludeCmdLine_Enabled Is Set" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "ProcessCreationIncludeCmdLine_Enabled Is Not Set" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing PowerShell Commandline Audting failed" | Tee-Object -FilePath $out_file -Append
}

$inf_str + "Testing if PowerShell Moduling is Enabled" | Tee-Object -FilePath $out_file -Append
Try{
    if ([bool](Get-ItemProperty -path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\PowerShell\ModuleLogging' -ErrorAction SilentlyContinue).EnableModuleLogging){
        $pos_str + "EnableModuleLogging Is Set" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "EnableModuleLogging Is Not Set" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    Try{
        if ([bool](Get-ItemProperty -path 'HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\PowerShell\ModuleLogging' -ErrorAction SilentlyContinue).EnableModuleLogging){
            $pos_str + "EnableModuleLogging Is Set" | Tee-Object -FilePath $out_file -Append
        } else {
            $neg_str + "EnableModuleLogging Is Not Set" | Tee-Object -FilePath $out_file -Append
        }
    }
    Catch{
        $err_str + "Testing PowerShell Moduling failed" | Tee-Object -FilePath $out_file -Append
    }
}

$inf_str + "Testing if PowerShell EnableScriptBlockLogging is Enabled" | Tee-Object -FilePath $out_file -Append
Try{
    if ([bool](Get-ItemProperty -path 'HKLM:\Software\Policies\Microsoft\Windows\PowerShell\ScriptBlockLogging' -ErrorAction SilentlyContinue).EnableScriptBlockLogging){
        $pos_str + "EnableScriptBlockLogging Is Set" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "EnableScriptBlockLogging Is Not Set" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    Try{
        if ([bool](Get-ItemProperty -path 'HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\PowerShell\ScriptBlockLogging' -ErrorAction SilentlyContinue).EnableScriptBlockLogging){
            $pos_str + "EnableScriptBlockLogging Is Set" | Tee-Object -FilePath $out_file -Append
        } else {
            $neg_str + "EnableScriptBlockLogging Is Not Set" | Tee-Object -FilePath $out_file -Append
        }
    }
    Catch{
        $err_str + "Testing PowerShell EnableScriptBlockLogging failed" | Tee-Object -FilePath $out_file -Append
    }
}

$inf_str + "Testing if PowerShell EnableScriptBlockInvocationLogging is Enabled" | Tee-Object -FilePath $out_file -Append
Try{
    if ([bool](Get-ItemProperty -path 'HKLM:\Software\Policies\Microsoft\Windows\PowerShell\ScriptBlockLogging' -ErrorAction SilentlyContinue).EnableScriptBlockInvocationLogging){
        $pos_str + "EnableScriptBlockInvocationLogging Is Set" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "EnableScriptBlockInvocationLogging Is Not Set" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    Try{
        if ([bool](Get-ItemProperty -path 'HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\PowerShell\ScriptBlockLogging' -ErrorAction SilentlyContinue).EnableScriptBlockInvocationLogging){
            $pos_str + "EnableScriptBlockInvocationLogging Is Set" | Tee-Object -FilePath $out_file -Append
        } else {
            $neg_str + "EnableScriptBlockInvocationLogging Is Not Set" | Tee-Object -FilePath $out_file -Append
        }
    }
    Catch{
        $err_str + "Testing PowerShell EnableScriptBlockInvocationLogging failed" | Tee-Object -FilePath $out_file -Append
    }
}

$inf_str + "Testing if PowerShell EnableTranscripting is Enabled" | Tee-Object -FilePath $out_file -Append
Try{
    if ([bool](Get-ItemProperty -path 'HKLM:\Software\Policies\Microsoft\Windows\PowerShell\Transcription' -ErrorAction SilentlyContinue).EnableTranscripting){
        $pos_str + "EnableTranscripting Is Set" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "EnableTranscripting Is Not Set" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    Try{
        if ([bool](Get-ItemProperty -path 'HKLM:\SOFTWARE\Wow6432Node\Policies\Microsoft\Windows\PowerShell\Transcription' -ErrorAction SilentlyContinue).EnableTranscripting){
            $pos_str + "EnableTranscripting Is Set" | Tee-Object -FilePath $out_file -Append
        } else {
            $neg_str + "EnableTranscripting Is Not Set" | Tee-Object -FilePath $out_file -Append
        }
    }
    Catch{
        $err_str + "Testing PowerShell EnableTranscripting failed" | Tee-Object -FilePath $out_file -Append
    }
}

$inf_str + "Testing if PowerShell EnableInvocationHeader is Enabled" | Tee-Object -FilePath $out_file -Append
Try{
    if ([bool](Get-ItemProperty -path 'HKLM:\Software\Policies\Microsoft\Windows\PowerShell\Transcription' -ErrorAction SilentlyContinue).EnableInvocationHeader){
        $pos_str + "EnableInvocationHeader Is Set" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "EnableInvocationHeader Is Not Set" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    Try{
        if ([bool](Get-ItemProperty -path 'HKLM:\Software\Wow6432Node\Policies\Microsoft\Windows\PowerShell\Transcription' -ErrorAction SilentlyContinue).EnableInvocationHeader){
            $pos_str + "EnableInvocationHeader Is Set" | Tee-Object -FilePath $out_file -Append
        } else {
            $neg_str + "EnableInvocationHeader Is Not Set" | Tee-Object -FilePath $out_file -Append
        }
    }
    Catch{
        $err_str + "Testing PowerShell EnableInvocationHeader failed" | Tee-Object -FilePath $out_file -Append
    }
}

$inf_str + "Testing if PowerShell ProtectedEventLogging is Enabled" | Tee-Object -FilePath $out_file -Append
Try{
    if ([bool](Get-ItemProperty -path 'HKLM:\Software\Policies\Microsoft\Windows\EventLog\ProtectedEventLogging' -ErrorAction SilentlyContinue).EnableProtectedEventLogging){
        $pos_str + "EnableProtectedEventLogging Is Set" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "EnableProtectedEventLogging Is Not Set" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    Try{
        if ([bool](Get-ItemProperty -path 'HKLM:\Software\Wow6432Node\Policies\Microsoft\Windows\EventLog\ProtectedEventLogging' -ErrorAction SilentlyContinue).EnableProtectedEventLogging){
            $pos_str + "EnableProtectedEventLogging Is Set" | Tee-Object -FilePath $out_file -Append
        } else {
            $neg_str + "EnableProtectedEventLogging Is Not Set" | Tee-Object -FilePath $out_file -Append
        }
    }
    Catch{
        $err_str + "Testing PowerShell ProtectedEventLogging failed" | Tee-Object -FilePath $out_file -Append
    }
}


########## Event Log Settings ##############
<#
Resources:
    Get-EventLog: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/get-eventlog?view=powershell-5.1
    Recommended settings for event log sizes in Windows: https://support.microsoft.com/en-us/help/957662/recommended-settings-for-event-log-sizes-in-windows
    Hey, Scripting Guy! How Can I Check the Size of My Event Log and Then Backup and Archive It If It Is More Than Half Full?: https://blogs.technet.microsoft.com/heyscriptingguy/2009/04/08/hey-scripting-guy-how-can-i-check-the-size-of-my-event-log-and-then-backup-and-archive-it-if-it-is-more-than-half-full/
    Is there a log file for RDP connections?: https://social.technet.microsoft.com/Forums/en-US/cb1c904f-b542-4102-a8cb-e0c464249280/is-there-a-log-file-for-rdp-connections?forum=winserverTS
    WINDOWS POWERSHELL LOGGING CHEAT SHEET - Win 7/Win 2008 or later: https://static1.squarespace.com/static/552092d5e4b0661088167e5c/t/5760096ecf80a129e0b17634/1465911664070/Windows+PowerShell+Logging+Cheat+Sheet+ver+June+2016+v2.pdf0
#>

$inf_str + "Event logs settings defaults are too small. Test that max sizes have been increased." | Tee-Object -FilePath $out_file -Append

$logs = @{
'Application' = 4
'System' = 4
'Security' = 4
'Windows PowerShell' = 4
'Microsoft-Windows-PowerShell/Operational' = 1
'Microsoft-Windows-TaskScheduler/Operational' = 1
'Microsoft-Windows-SMBServer/Audit' = 1
'Microsoft-Windows-WinRM/Operational' = 1
'Microsoft-Windows-WMI-Activity/Operational' = 1
'Microsoft-AppV-Client-Streamingux/Debug' = 1
'Microsoft-AppV-Client/Admin' = 1
'Microsoft-AppV-Client/Debug' = 1
'Microsoft-AppV-Client/Operational' = 1
'Microsoft-AppV-Client/Virtual Applications' = 1
'Microsoft-AppV-SharedPerformance/Analytic' = 1
'Microsoft-Client-Licensing-Platform/Admin' = 1
'Microsoft-Client-Licensing-Platform/Debug' = 1
'Microsoft-Client-Licensing-Platform/Diagnostic' = 1
'Microsoft-IE/Diagnostic' = 1
'Microsoft-IEFRAME/Diagnostic' = 1
'Microsoft-JSDumpHeap/Diagnostic' = 1
'Microsoft-OneCore-Setup/Analytic' = 1
'Microsoft-PerfTrack-IEFRAME/Diagnostic' = 1
'Microsoft-PerfTrack-MSHTML/Diagnostic' = 1
'Microsoft-User Experience Virtualization-Admin/Debug' = 1
'Microsoft-User Experience Virtualization-Agent Driver/Debug' = 1
'Microsoft-User Experience Virtualization-Agent Driver/Operational' = 1
'Microsoft-User Experience Virtualization-App Agent/Analytic' = 1
'Microsoft-User Experience Virtualization-App Agent/Debug' = 1
'Microsoft-User Experience Virtualization-App Agent/Operational' = 1
'Microsoft-User Experience Virtualization-IPC/Operational' = 1
'Microsoft-User Experience Virtualization-SQM Uploader/Analytic' = 1
'Microsoft-User Experience Virtualization-SQM Uploader/Debug' = 1
'Microsoft-User Experience Virtualization-SQM Uploader/Operational' = 1
'Microsoft-Windows-AAD/Analytic' = 1
'Microsoft-Windows-AAD/Operational' = 1
'Microsoft-Windows-ADSI/Debug' = 1
'Microsoft-Windows-ASN1/Operational' = 1
'Microsoft-Windows-ATAPort/General' = 1
'Microsoft-Windows-ATAPort/SATA-LPM' = 1
'Microsoft-Windows-ActionQueue/Analytic' = 1
'Microsoft-Windows-All-User-Install-Agent/Admin' = 1
'Microsoft-Windows-AllJoyn/Debug' = 1
'Microsoft-Windows-AllJoyn/Operational' = 1
'Microsoft-Windows-AppHost/Admin' = 1
'Microsoft-Windows-AppHost/ApplicationTracing' = 1
'Microsoft-Windows-AppHost/Diagnostic' = 1
'Microsoft-Windows-AppHost/Internal' = 1
'Microsoft-Windows-AppID/Operational' = 1
'Microsoft-Windows-AppLocker/EXE and DLL' = 1
'Microsoft-Windows-AppLocker/MSI and Script' = 1
'Microsoft-Windows-AppLocker/Packaged app-Deployment' = 1
'Microsoft-Windows-AppLocker/Packaged app-Execution' = 1
'Microsoft-Windows-AppModel-Runtime/Admin' = 1
'Microsoft-Windows-AppModel-Runtime/Analytic' = 1
'Microsoft-Windows-AppModel-Runtime/Debug' = 1
'Microsoft-Windows-AppModel-Runtime/Diagnostics' = 1
'Microsoft-Windows-AppModel-State/Debug' = 1
'Microsoft-Windows-AppModel-State/Diagnostic' = 1
'Microsoft-Windows-AppReadiness/Admin' = 1
'Microsoft-Windows-AppReadiness/Debug' = 1
'Microsoft-Windows-AppReadiness/Operational' = 1
'Microsoft-Windows-AppSruProv' = 1
'Microsoft-Windows-AppXDeployment/Diagnostic' = 1
'Microsoft-Windows-AppXDeployment/Operational' = 1
'Microsoft-Windows-AppXDeploymentServer/Debug' = 1
'Microsoft-Windows-AppXDeploymentServer/Diagnostic' = 1
'Microsoft-Windows-AppXDeploymentServer/Operational' = 1
'Microsoft-Windows-AppXDeploymentServer/Restricted' = 1
'Microsoft-Windows-ApplicabilityEngine/Analytic' = 1
'Microsoft-Windows-ApplicabilityEngine/Operational' = 1
'Microsoft-Windows-Application Server-Applications/Admin' = 1
'Microsoft-Windows-Application Server-Applications/Analytic' = 1
'Microsoft-Windows-Application Server-Applications/Debug' = 1
'Microsoft-Windows-Application Server-Applications/Operational' = 1
'Microsoft-Windows-Application-Experience/Compatibility-Infrastructure-Debug' = 1
'Microsoft-Windows-Application-Experience/Program-Compatibility-Assistant' = 1
'Microsoft-Windows-Application-Experience/Program-Compatibility-Assistant/Analytic' = 1
'Microsoft-Windows-Application-Experience/Program-Compatibility-Assistant/Trace' = 1
'Microsoft-Windows-Application-Experience/Program-Compatibility-Troubleshooter' = 1
'Microsoft-Windows-Application-Experience/Program-Inventory' = 1
'Microsoft-Windows-Application-Experience/Program-Telemetry' = 1
'Microsoft-Windows-Application-Experience/Steps-Recorder' = 1
'Microsoft-Windows-AppxPackaging/Debug' = 1
'Microsoft-Windows-AppxPackaging/Operational' = 1
'Microsoft-Windows-AppxPackaging/Performance' = 1
'Microsoft-Windows-AssignedAccess/Admin' = 1
'Microsoft-Windows-AssignedAccess/Operational' = 1
'Microsoft-Windows-AssignedAccessBroker/Admin' = 1
'Microsoft-Windows-AssignedAccessBroker/Operational' = 1
'Microsoft-Windows-AsynchronousCausality/Causality' = 1
'Microsoft-Windows-Audio/CaptureMonitor' = 1
'Microsoft-Windows-Audio/GlitchDetection' = 1
'Microsoft-Windows-Audio/Informational' = 1
'Microsoft-Windows-Audio/Operational' = 1
'Microsoft-Windows-Audio/Performance' = 1
'Microsoft-Windows-Audio/PlaybackManager' = 1
'Microsoft-Windows-Audit/Analytic' = 1
'Microsoft-Windows-Authentication User Interface/Operational' = 1
'Microsoft-Windows-Authentication/AuthenticationPolicyFailures-DomainController' = 1
'Microsoft-Windows-Authentication/ProtectedUser-Client' = 1
'Microsoft-Windows-Authentication/ProtectedUserFailures-DomainController' = 1
'Microsoft-Windows-Authentication/ProtectedUserSuccesses-DomainController' = 1
'Microsoft-Windows-AxInstallService/Log' = 1
'Microsoft-Windows-BTH-BTHPORT/HCI' = 1
'Microsoft-Windows-BTH-BTHPORT/L2CAP' = 1
'Microsoft-Windows-BTH-BTHUSB/Diagnostic' = 1
'Microsoft-Windows-BTH-BTHUSB/Performance' = 1
'Microsoft-Windows-BackgroundTaskInfrastructure/Diagnostic' = 1
'Microsoft-Windows-BackgroundTaskInfrastructure/Operational' = 1
'Microsoft-Windows-BackgroundTransfer-ContentPrefetcher/Operational' = 1
'Microsoft-Windows-Backup' = 1
'Microsoft-Windows-Base-Filtering-Engine-Connections/Operational' = 1
'Microsoft-Windows-Base-Filtering-Engine-Resource-Flows/Operational' = 1
'Microsoft-Windows-Battery/Diagnostic' = 1
'Microsoft-Windows-Biometrics/Analytic' = 1
'Microsoft-Windows-Biometrics/Operational' = 1
'Microsoft-Windows-BitLocker-DrivePreparationTool/Admin' = 1
'Microsoft-Windows-BitLocker-DrivePreparationTool/Operational' = 1
'Microsoft-Windows-BitLocker-Driver-Performance/Operational' = 1
'Microsoft-Windows-BitLocker/BitLocker Management' = 1
'Microsoft-Windows-BitLocker/BitLocker Operational' = 1
'Microsoft-Windows-BitLocker/Tracing' = 1
'Microsoft-Windows-Bits-Client/Analytic' = 1
'Microsoft-Windows-Bits-Client/Operational' = 1
'Microsoft-Windows-Bluetooth-BthLEPrepairing/Operational' = 1
'Microsoft-Windows-Bluetooth-Bthmini/Operational' = 1
'Microsoft-Windows-Bluetooth-Policy/Operational' = 1
'Microsoft-Windows-BranchCache/Operational' = 1
'Microsoft-Windows-BranchCacheClientEventProvider/Diagnostic' = 1
'Microsoft-Windows-BranchCacheEventProvider/Diagnostic' = 1
'Microsoft-Windows-BranchCacheMonitoring/Analytic' = 1
'Microsoft-Windows-BranchCacheSMB/Analytic' = 1
'Microsoft-Windows-BranchCacheSMB/Operational' = 1
'Microsoft-Windows-CAPI2/Catalog Database Debug' = 1
'Microsoft-Windows-CAPI2/Operational' = 1
'Microsoft-Windows-CDROM/Operational' = 1
'Microsoft-Windows-COM/Analytic' = 1
'Microsoft-Windows-COM/ApartmentInitialize' = 1
'Microsoft-Windows-COM/ApartmentUninitialize' = 1
'Microsoft-Windows-COM/Call' = 1
'Microsoft-Windows-COM/CreateInstance' = 1
'Microsoft-Windows-COM/ExtensionCatalog' = 1
'Microsoft-Windows-COM/FreeUnusedLibrary' = 1
'Microsoft-Windows-COM/RundownInstrumentation' = 1
'Microsoft-Windows-COMRuntime/Activations' = 1
'Microsoft-Windows-COMRuntime/MessageProcessing' = 1
'Microsoft-Windows-COMRuntime/Tracing' = 1
'Microsoft-Windows-CertPoleEng/Operational' = 1
'Microsoft-Windows-CertificateServicesClient-CredentialRoaming/Operational' = 1
'Microsoft-Windows-CertificateServicesClient-Lifecycle-System/Operational' = 1
'Microsoft-Windows-CertificateServicesClient-Lifecycle-User/Operational' = 1
'Microsoft-Windows-ClearTypeTextTuner/Diagnostic' = 1
'Microsoft-Windows-CloudStore/Debug' = 1
'Microsoft-Windows-CloudStore/Operational' = 1
'Microsoft-Windows-CmiSetup/Analytic' = 1
'Microsoft-Windows-CodeIntegrity/Operational' = 1
'Microsoft-Windows-CodeIntegrity/Verbose' = 1
'Microsoft-Windows-ComDlg32/Analytic' = 1
'Microsoft-Windows-ComDlg32/Debug' = 1
'Microsoft-Windows-Compat-Appraiser/Analytic' = 1
'Microsoft-Windows-Compat-Appraiser/Operational' = 1
'Microsoft-Windows-Containers-BindFlt/Debug' = 1
'Microsoft-Windows-Containers-BindFlt/Operational' = 1
'Microsoft-Windows-Containers-Wcifs/Debug' = 1
'Microsoft-Windows-Containers-Wcifs/Operational' = 1
'Microsoft-Windows-Containers-Wcnfs/Debug' = 1
'Microsoft-Windows-Containers-Wcnfs/Operational' = 1
'Microsoft-Windows-CoreApplication/Diagnostic' = 1
'Microsoft-Windows-CoreApplication/Operational' = 1
'Microsoft-Windows-CoreApplication/Tracing' = 1
'Microsoft-Windows-CoreSystem-SmsRouter-Events/Debug' = 1
'Microsoft-Windows-CoreSystem-SmsRouter-Events/Operational' = 1
'Microsoft-Windows-CoreWindow/Analytic' = 1
'Microsoft-Windows-CoreWindow/Debug' = 1
'Microsoft-Windows-CorruptedFileRecovery-Client/Operational' = 1
'Microsoft-Windows-CorruptedFileRecovery-Server/Operational' = 1
'Microsoft-Windows-Crashdump/Operational' = 1
'Microsoft-Windows-CredUI/Diagnostic' = 1
'Microsoft-Windows-Crypto-BCRYPT/Analytic' = 1
'Microsoft-Windows-Crypto-CNG/Analytic' = 1
'Microsoft-Windows-Crypto-DPAPI/BackUpKeySvc' = 1
'Microsoft-Windows-Crypto-DPAPI/Debug' = 1
'Microsoft-Windows-Crypto-DPAPI/Operational' = 1
'Microsoft-Windows-Crypto-DSSEnh/Analytic' = 1
'Microsoft-Windows-Crypto-NCrypt/Operational' = 1
'Microsoft-Windows-Crypto-RNG/Analytic' = 1
'Microsoft-Windows-Crypto-RSAEnh/Analytic' = 1
'Microsoft-Windows-D3D10Level9/Analytic' = 1
'Microsoft-Windows-D3D10Level9/PerfTiming' = 1
'Microsoft-Windows-DAL-Provider/Analytic' = 1
'Microsoft-Windows-DAL-Provider/Operational' = 1
'Microsoft-Windows-DAMM/Diagnostic' = 1
'Microsoft-Windows-DCLocator/Debug' = 1
'Microsoft-Windows-DDisplay/Analytic' = 1
'Microsoft-Windows-DDisplay/Logging' = 1
'Microsoft-Windows-DNS-Client/Operational' = 1
'Microsoft-Windows-DSC/Admin' = 1
'Microsoft-Windows-DSC/Analytic' = 1
'Microsoft-Windows-DSC/Debug' = 1
'Microsoft-Windows-DSC/Operational' = 1
'Microsoft-Windows-DUI/Diagnostic' = 1
'Microsoft-Windows-DUSER/Diagnostic' = 1
'Microsoft-Windows-DXGI/Analytic' = 1
'Microsoft-Windows-DXGI/Logging' = 1
'Microsoft-Windows-DXP/Analytic' = 1
'Microsoft-Windows-Data-Pdf/Debug' = 1
'Microsoft-Windows-DataIntegrityScan/Admin' = 1
'Microsoft-Windows-DataIntegrityScan/CrashRecovery' = 1
'Microsoft-Windows-DateTimeControlPanel/Analytic' = 1
'Microsoft-Windows-DateTimeControlPanel/Debug' = 1
'Microsoft-Windows-DateTimeControlPanel/Operational' = 1
'Microsoft-Windows-Deduplication/Diagnostic' = 1
'Microsoft-Windows-Deduplication/Operational' = 1
'Microsoft-Windows-Deduplication/Performance' = 1
'Microsoft-Windows-Deduplication/Scrubbing' = 1
'Microsoft-Windows-Defrag-Core/Debug' = 1
'Microsoft-Windows-Deplorch/Analytic' = 1
'Microsoft-Windows-DesktopActivityModerator/Diagnostic' = 1
'Microsoft-Windows-DesktopWindowManager-Diag/Diagnostic' = 1
'Microsoft-Windows-DeviceAssociationService/Performance' = 1
'Microsoft-Windows-DeviceConfidence/Analytic' = 1
'Microsoft-Windows-DeviceGuard/Operational' = 1
'Microsoft-Windows-DeviceGuard/Verbose' = 1
'Microsoft-Windows-DeviceManagement-Enterprise-Diagnostics-Provider/Admin' = 1
'Microsoft-Windows-DeviceManagement-Enterprise-Diagnostics-Provider/Debug' = 1
'Microsoft-Windows-DeviceManagement-Enterprise-Diagnostics-Provider/Operational' = 1
'Microsoft-Windows-DeviceSetupManager/Admin' = 1
'Microsoft-Windows-DeviceSetupManager/Analytic' = 1
'Microsoft-Windows-DeviceSetupManager/Debug' = 1
'Microsoft-Windows-DeviceSetupManager/Operational' = 1
'Microsoft-Windows-DeviceSync/Analytic' = 1
'Microsoft-Windows-DeviceSync/Operational' = 1
'Microsoft-Windows-DeviceUpdateAgent/Operational' = 1
'Microsoft-Windows-DeviceUx/Informational' = 1
'Microsoft-Windows-DeviceUx/Performance' = 1
'Microsoft-Windows-Devices-Background/Operational' = 1
'Microsoft-Windows-Dhcp-Client/Admin' = 1
'Microsoft-Windows-Dhcp-Client/Operational' = 1
'Microsoft-Windows-Dhcpv6-Client/Admin' = 1
'Microsoft-Windows-Dhcpv6-Client/Operational' = 1
'Microsoft-Windows-DiagCpl/Debug' = 1
'Microsoft-Windows-Diagnosis-AdvancedTaskManager/Analytic' = 1
'Microsoft-Windows-Diagnosis-DPS/Analytic' = 1
'Microsoft-Windows-Diagnosis-DPS/Debug' = 1
'Microsoft-Windows-Diagnosis-DPS/Operational' = 1
'Microsoft-Windows-Diagnosis-MSDE/Debug' = 1
'Microsoft-Windows-Diagnosis-PCW/Analytic' = 1
'Microsoft-Windows-Diagnosis-PCW/Debug' = 1
'Microsoft-Windows-Diagnosis-PCW/Operational' = 1
'Microsoft-Windows-Diagnosis-PLA/Debug' = 1
'Microsoft-Windows-Diagnosis-PLA/Operational' = 1
'Microsoft-Windows-Diagnosis-Perfhost/Analytic' = 1
'Microsoft-Windows-Diagnosis-Scheduled/Operational' = 1
'Microsoft-Windows-Diagnosis-Scripted/Admin' = 1
'Microsoft-Windows-Diagnosis-Scripted/Analytic' = 1
'Microsoft-Windows-Diagnosis-Scripted/Debug' = 1
'Microsoft-Windows-Diagnosis-Scripted/Operational' = 1
'Microsoft-Windows-Diagnosis-ScriptedDiagnosticsProvider/Debug' = 1
'Microsoft-Windows-Diagnosis-ScriptedDiagnosticsProvider/Operational' = 1
'Microsoft-Windows-Diagnosis-WDC/Analytic' = 1
'Microsoft-Windows-Diagnosis-WDI/Debug' = 1
'Microsoft-Windows-Diagnostics-Networking/Debug' = 1
'Microsoft-Windows-Diagnostics-Networking/Operational' = 1
'Microsoft-Windows-Diagnostics-PerfTrack-Counters/Diagnostic' = 1
'Microsoft-Windows-Diagnostics-PerfTrack/Diagnostic' = 1
'Microsoft-Windows-Diagnostics-Performance/Diagnostic' = 1
'Microsoft-Windows-Diagnostics-Performance/Diagnostic/Loopback' = 1
'Microsoft-Windows-Diagnostics-Performance/Operational' = 1
'Microsoft-Windows-Direct3D10/Analytic' = 1
'Microsoft-Windows-Direct3D10_1/Analytic' = 1
'Microsoft-Windows-Direct3D11/Analytic' = 1
'Microsoft-Windows-Direct3D11/Logging' = 1
'Microsoft-Windows-Direct3D11/PerfTiming' = 1
'Microsoft-Windows-Direct3D12/Analytic' = 1
'Microsoft-Windows-Direct3D12/Logging' = 1
'Microsoft-Windows-Direct3D12/PerfTiming' = 1
'Microsoft-Windows-Direct3D9/Analytic' = 1
'Microsoft-Windows-Direct3DShaderCache/Default' = 1
'Microsoft-Windows-DirectComposition/Diagnostic' = 1
'Microsoft-Windows-DirectManipulation/Diagnostic' = 1
'Microsoft-Windows-DirectShow-KernelSupport/Performance' = 1
'Microsoft-Windows-DirectSound/Debug' = 1
'Microsoft-Windows-Disk/Operational' = 1
'Microsoft-Windows-DiskDiagnostic/Operational' = 1
'Microsoft-Windows-DiskDiagnosticDataCollector/Operational' = 1
'Microsoft-Windows-DiskDiagnosticResolver/Operational' = 1
'Microsoft-Windows-Dism-Api/Analytic' = 1
'Microsoft-Windows-Dism-Api/ExternalAnalytic' = 1
'Microsoft-Windows-Dism-Api/InternalAnalytic' = 1
'Microsoft-Windows-Dism-Cli/Analytic' = 1
'Microsoft-Windows-DisplayColorCalibration/Debug' = 1
'Microsoft-Windows-DisplayColorCalibration/Operational' = 1
'Microsoft-Windows-DisplaySwitch/Diagnostic' = 1
'Microsoft-Windows-Documents/Performance' = 1
'Microsoft-Windows-Dot3MM/Diagnostic' = 1
'Microsoft-Windows-DriverFrameworks-UserMode/Operational' = 1
'Microsoft-Windows-Dwm-API/Diagnostic' = 1
'Microsoft-Windows-Dwm-Core/Diagnostic' = 1
'Microsoft-Windows-Dwm-Dwm/Diagnostic' = 1
'Microsoft-Windows-Dwm-Redir/Diagnostic' = 1
'Microsoft-Windows-Dwm-Udwm/Diagnostic' = 1
'Microsoft-Windows-DxgKrnl-Admin' = 1
'Microsoft-Windows-DxgKrnl-Operational' = 1
'Microsoft-Windows-DxgKrnl/Contention' = 1
'Microsoft-Windows-DxgKrnl/Diagnostic' = 1
'Microsoft-Windows-DxgKrnl/Performance' = 1
'Microsoft-Windows-DxgKrnl/Power' = 1
'Microsoft-Windows-DxpTaskSyncProvider/Analytic' = 1
'Microsoft-Windows-EDP-Application-Learning/Admin' = 1
'Microsoft-Windows-EDP-Audit-Regular/Admin' = 1
'Microsoft-Windows-EDP-Audit-TCB/Admin' = 1
'Microsoft-Windows-EFS/Debug' = 1
'Microsoft-Windows-ESE/IODiagnose' = 1
'Microsoft-Windows-ESE/Operational' = 1
'Microsoft-Windows-EapHost/Analytic' = 1
'Microsoft-Windows-EapHost/Debug' = 1
'Microsoft-Windows-EapHost/Operational' = 1
'Microsoft-Windows-EapMethods-RasChap/Operational' = 1
'Microsoft-Windows-EapMethods-RasTls/Operational' = 1
'Microsoft-Windows-EapMethods-Sim/Operational' = 1
'Microsoft-Windows-EapMethods-Ttls/Operational' = 1
'Microsoft-Windows-EaseOfAccess/Diagnostic' = 1
'Microsoft-Windows-Energy-Estimation-Engine/EventLog' = 1
'Microsoft-Windows-Energy-Estimation-Engine/Trace' = 1
'Microsoft-Windows-EnhancedStorage-EhStorTcgDrv/Analytic' = 1
'Microsoft-Windows-EventCollector/Debug' = 1
'Microsoft-Windows-EventCollector/Operational' = 1
'Microsoft-Windows-EventLog-WMIProvider/Debug' = 1
'Microsoft-Windows-EventLog/Analytic' = 1
'Microsoft-Windows-EventLog/Debug' = 1
'Microsoft-Windows-FMS/Analytic' = 1
'Microsoft-Windows-FMS/Debug' = 1
'Microsoft-Windows-FMS/Operational' = 1
'Microsoft-Windows-FailoverClustering-Client/Diagnostic' = 1
'Microsoft-Windows-Fault-Tolerant-Heap/Operational' = 1
'Microsoft-Windows-FeatureConfiguration/Analytic' = 1
'Microsoft-Windows-FeatureConfiguration/Operational' = 1
'Microsoft-Windows-FileHistory-Catalog/Analytic' = 1
'Microsoft-Windows-FileHistory-Catalog/Debug' = 1
'Microsoft-Windows-FileHistory-ConfigManager/Analytic' = 1
'Microsoft-Windows-FileHistory-ConfigManager/Debug' = 1
'Microsoft-Windows-FileHistory-Core/Analytic' = 1
'Microsoft-Windows-FileHistory-Core/Debug' = 1
'Microsoft-Windows-FileHistory-Core/WHC' = 1
'Microsoft-Windows-FileHistory-Engine/Analytic' = 1
'Microsoft-Windows-FileHistory-Engine/BackupLog' = 1
'Microsoft-Windows-FileHistory-Engine/Debug' = 1
'Microsoft-Windows-FileHistory-EventListener/Analytic' = 1
'Microsoft-Windows-FileHistory-EventListener/Debug' = 1
'Microsoft-Windows-FileHistory-Service/Analytic' = 1
'Microsoft-Windows-FileHistory-Service/Debug' = 1
'Microsoft-Windows-FileHistory-UI-Events/Analytic' = 1
'Microsoft-Windows-FileHistory-UI-Events/Debug' = 1
'Microsoft-Windows-FileInfoMinifilter/Operational' = 1
'Microsoft-Windows-Firewall-CPL/Diagnostic' = 1
'Microsoft-Windows-Folder Redirection/Operational' = 1
'Microsoft-Windows-Forwarding/Debug' = 1
'Microsoft-Windows-Forwarding/Operational' = 1
'Microsoft-Windows-GPIO-ClassExtension/Analytic' = 1
'Microsoft-Windows-GenericRoaming/Admin' = 1
'Microsoft-Windows-GroupPolicy/Operational' = 1
'Microsoft-Windows-HAL/Debug' = 1
'Microsoft-Windows-HealthCenter/Debug' = 1
'Microsoft-Windows-HealthCenter/Performance' = 1
'Microsoft-Windows-HealthCenterCPL/Performance' = 1
'Microsoft-Windows-HelloForBusiness/Operational' = 1
'Microsoft-Windows-Help/Operational' = 1
'Microsoft-Windows-HomeGroup Control Panel Performance/Diagnostic' = 1
'Microsoft-Windows-HomeGroup Control Panel/Operational' = 1
'Microsoft-Windows-HomeGroup Listener Service/Operational' = 1
'Microsoft-Windows-HomeGroup Provider Service Performance/Diagnostic' = 1
'Microsoft-Windows-HomeGroup Provider Service/Operational' = 1
'Microsoft-Windows-HomeGroup-ListenerService' = 1
'Microsoft-Windows-HotspotAuth/Analytic' = 1
'Microsoft-Windows-HotspotAuth/Operational' = 1
'Microsoft-Windows-HttpService/Log' = 1
'Microsoft-Windows-HttpService/Trace' = 1
'Microsoft-Windows-Hyper-V-Guest-Drivers/Admin' = 1
'Microsoft-Windows-Hyper-V-Guest-Drivers/Analytic' = 1
'Microsoft-Windows-Hyper-V-Guest-Drivers/Debug' = 1
'Microsoft-Windows-Hyper-V-Guest-Drivers/Diagnose' = 1
'Microsoft-Windows-Hyper-V-Guest-Drivers/Operational' = 1
'Microsoft-Windows-Hyper-V-Hypervisor-Admin' = 1
'Microsoft-Windows-Hyper-V-Hypervisor-Analytic' = 1
'Microsoft-Windows-Hyper-V-Hypervisor-Operational' = 1
'Microsoft-Windows-Hyper-V-NETVSC/Diagnostic' = 1
'Microsoft-Windows-Hyper-V-VID-Admin' = 1
'Microsoft-Windows-Hyper-V-VID-Analytic' = 1
'Microsoft-Windows-IE-SmartScreen' = 1
'Microsoft-Windows-IKE/Operational' = 1
'Microsoft-Windows-IKEDBG/Debug' = 1
'Microsoft-Windows-IME-Broker/Analytic' = 1
'Microsoft-Windows-IME-CandidateUI/Analytic' = 1
'Microsoft-Windows-IME-CustomerFeedbackManager/Debug' = 1
'Microsoft-Windows-IME-CustomerFeedbackManagerUI/Analytic' = 1
'Microsoft-Windows-IME-JPAPI/Analytic' = 1
'Microsoft-Windows-IME-JPLMP/Analytic' = 1
'Microsoft-Windows-IME-JPPRED/Analytic' = 1
'Microsoft-Windows-IME-JPSetting/Analytic' = 1
'Microsoft-Windows-IME-JPTIP/Analytic' = 1
'Microsoft-Windows-IME-KRAPI/Analytic' = 1
'Microsoft-Windows-IME-KRTIP/Analytic' = 1
'Microsoft-Windows-IME-OEDCompiler/Analytic' = 1
'Microsoft-Windows-IME-TCCORE/Analytic' = 1
'Microsoft-Windows-IME-TCTIP/Analytic' = 1
'Microsoft-Windows-IME-TIP/Analytic' = 1
'Microsoft-Windows-IPNAT/Diagnostic' = 1
'Microsoft-Windows-IPSEC-SRV/Diagnostic' = 1
'Microsoft-Windows-IPxlatCfg/Debug' = 1
'Microsoft-Windows-IPxlatCfg/Operational' = 1
'Microsoft-Windows-IdCtrls/Analytic' = 1
'Microsoft-Windows-IdCtrls/Operational' = 1
'Microsoft-Windows-IndirectDisplays-ClassExtension-Events/Diagnostic' = 1
'Microsoft-Windows-Input-HIDCLASS-Analytic' = 1
'Microsoft-Windows-InputSwitch/Diagnostic' = 1
'Microsoft-Windows-International-RegionalOptionsControlPanel/Operational' = 1
'Microsoft-Windows-International/Operational' = 1
'Microsoft-Windows-Iphlpsvc/Debug' = 1
'Microsoft-Windows-Iphlpsvc/Operational' = 1
'Microsoft-Windows-Iphlpsvc/Trace' = 1
'Microsoft-Windows-KdsSvc/Operational' = 1
'Microsoft-Windows-Kerberos/Operational' = 1
'Microsoft-Windows-Kernel-Acpi/Diagnostic' = 1
'Microsoft-Windows-Kernel-AppCompat/General' = 1
'Microsoft-Windows-Kernel-AppCompat/Performance' = 1
'Microsoft-Windows-Kernel-ApphelpCache/Analytic' = 1
'Microsoft-Windows-Kernel-ApphelpCache/Debug' = 1
'Microsoft-Windows-Kernel-ApphelpCache/Operational' = 1
'Microsoft-Windows-Kernel-Boot/Analytic' = 1
'Microsoft-Windows-Kernel-Boot/Operational' = 1
'Microsoft-Windows-Kernel-BootDiagnostics/Diagnostic' = 1
'Microsoft-Windows-Kernel-Disk/Analytic' = 1
'Microsoft-Windows-Kernel-EventTracing/Admin' = 1
'Microsoft-Windows-Kernel-EventTracing/Analytic' = 1
'Microsoft-Windows-Kernel-File/Analytic' = 1
'Microsoft-Windows-Kernel-IO/Operational' = 1
'Microsoft-Windows-Kernel-Interrupt-Steering/Diagnostic' = 1
'Microsoft-Windows-Kernel-IoTrace/Diagnostic' = 1
'Microsoft-Windows-Kernel-LiveDump/Analytic' = 1
'Microsoft-Windows-Kernel-Memory/Analytic' = 1
'Microsoft-Windows-Kernel-Network/Analytic' = 1
'Microsoft-Windows-Kernel-Pdc/Diagnostic' = 1
'Microsoft-Windows-Kernel-Pep/Diagnostic' = 1
'Microsoft-Windows-Kernel-PnP/Boot Diagnostic' = 1
'Microsoft-Windows-Kernel-PnP/Configuration' = 1
'Microsoft-Windows-Kernel-PnP/Configuration Diagnostic' = 1
'Microsoft-Windows-Kernel-PnP/Device Enumeration Diagnostic' = 1
'Microsoft-Windows-Kernel-PnP/Driver Diagnostic' = 1
'Microsoft-Windows-Kernel-Power/Diagnostic' = 1
'Microsoft-Windows-Kernel-Power/Thermal-Diagnostic' = 1
'Microsoft-Windows-Kernel-Power/Thermal-Operational' = 1
'Microsoft-Windows-Kernel-Prefetch/Diagnostic' = 1
'Microsoft-Windows-Kernel-Process/Analytic' = 1
'Microsoft-Windows-Kernel-Processor-Power/Diagnostic' = 1
'Microsoft-Windows-Kernel-Registry/Analytic' = 1
'Microsoft-Windows-Kernel-Registry/Performance' = 1
'Microsoft-Windows-Kernel-ShimEngine/Debug' = 1
'Microsoft-Windows-Kernel-ShimEngine/Diagnostic' = 1
'Microsoft-Windows-Kernel-ShimEngine/Operational' = 1
'Microsoft-Windows-Kernel-StoreMgr/Analytic' = 1
'Microsoft-Windows-Kernel-StoreMgr/Operational' = 1
'Microsoft-Windows-Kernel-WDI/Analytic' = 1
'Microsoft-Windows-Kernel-WDI/Debug' = 1
'Microsoft-Windows-Kernel-WDI/Operational' = 1
'Microsoft-Windows-Kernel-WHEA/Errors' = 1
'Microsoft-Windows-Kernel-WHEA/Operational' = 1
'Microsoft-Windows-Kernel-XDV/Analytic' = 1
'Microsoft-Windows-Known Folders API Service' = 1
'Microsoft-Windows-L2NA/Diagnostic' = 1
'Microsoft-Windows-LDAP-Client/Debug' = 1
'Microsoft-Windows-LSA/Diagnostic' = 1
'Microsoft-Windows-LSA/Operational' = 1
'Microsoft-Windows-LSA/Performance' = 1
'Microsoft-Windows-LUA-ConsentUI/Diagnostic' = 1
'Microsoft-Windows-LanguagePackSetup/Analytic' = 1
'Microsoft-Windows-LanguagePackSetup/Debug' = 1
'Microsoft-Windows-LanguagePackSetup/Operational' = 1
'Microsoft-Windows-LimitsManagement/Diagnostic' = 1
'Microsoft-Windows-LinkLayerDiscoveryProtocol/Diagnostic' = 1
'Microsoft-Windows-LinkLayerDiscoveryProtocol/Operational' = 1
'Microsoft-Windows-LiveId/Analytic' = 1
'Microsoft-Windows-LiveId/Operational' = 1
'Microsoft-Windows-MPS-CLNT/Diagnostic' = 1
'Microsoft-Windows-MPS-DRV/Diagnostic' = 1
'Microsoft-Windows-MPS-SRV/Diagnostic' = 1
'Microsoft-Windows-MSFTEDIT/Diagnostic' = 1
'Microsoft-Windows-MSPaint/Admin' = 1
'Microsoft-Windows-MSPaint/Debug' = 1
'Microsoft-Windows-MSPaint/Diagnostic' = 1
'Microsoft-Windows-MUI/Admin' = 1
'Microsoft-Windows-MUI/Analytic' = 1
'Microsoft-Windows-MUI/Debug' = 1
'Microsoft-Windows-MUI/Operational' = 1
'Microsoft-Windows-MemoryDiagnostics-Results/Debug' = 1
'Microsoft-Windows-Minstore/Analytic' = 1
'Microsoft-Windows-Minstore/Debug' = 1
'Microsoft-Windows-Mobile-Broadband-Experience-Api-Internal/Analytic' = 1
'Microsoft-Windows-Mobile-Broadband-Experience-Api/Analytic' = 1
'Microsoft-Windows-Mobile-Broadband-Experience-Parser-Task/Analytic' = 1
'Microsoft-Windows-Mobile-Broadband-Experience-Parser-Task/Operational' = 1
'Microsoft-Windows-Mobile-Broadband-Experience-SmsApi/Analytic' = 1
'Microsoft-Windows-MobilityCenter/Performance' = 1
'Microsoft-Windows-ModernDeployment-Diagnostics-Provider/Admin' = 1
'Microsoft-Windows-ModernDeployment-Diagnostics-Provider/Autopilot' = 1
'Microsoft-Windows-ModernDeployment-Diagnostics-Provider/Debug' = 1
'Microsoft-Windows-ModernDeployment-Diagnostics-Provider/ManagementService' = 1
'Microsoft-Windows-Mprddm/Operational' = 1
'Microsoft-Windows-NCSI/Analytic' = 1
'Microsoft-Windows-NCSI/Operational' = 1
'Microsoft-Windows-NDF-HelperClassDiscovery/Debug' = 1
'Microsoft-Windows-NDIS-PacketCapture/Diagnostic' = 1
'Microsoft-Windows-NDIS/Diagnostic' = 1
'Microsoft-Windows-NDIS/Operational' = 1
'Microsoft-Windows-NFC-Class-Extension/Analytical' = 1
'Microsoft-Windows-NTLM/Operational' = 1
'Microsoft-Windows-NWiFi/Diagnostic' = 1
'Microsoft-Windows-Narrator/Diagnostic' = 1
'Microsoft-Windows-Ncasvc/Operational' = 1
'Microsoft-Windows-NcdAutoSetup/Diagnostic' = 1
'Microsoft-Windows-NcdAutoSetup/Operational' = 1
'Microsoft-Windows-NdisImPlatform/Operational' = 1
'Microsoft-Windows-Ndu/Diagnostic' = 1
'Microsoft-Windows-NetShell/Performance' = 1
'Microsoft-Windows-Network-Connection-Broker' = 1
'Microsoft-Windows-Network-DataUsage/Analytic' = 1
'Microsoft-Windows-Network-Setup/Diagnostic' = 1
'Microsoft-Windows-Network-and-Sharing-Center/Diagnostic' = 1
'Microsoft-Windows-NetworkBridge/Diagnostic' = 1
'Microsoft-Windows-NetworkLocationWizard/Operational' = 1
'Microsoft-Windows-NetworkProfile/Diagnostic' = 1
'Microsoft-Windows-NetworkProfile/Operational' = 1
'Microsoft-Windows-NetworkProvider/Operational' = 1
'Microsoft-Windows-NetworkProvisioning/Analytic' = 1
'Microsoft-Windows-NetworkProvisioning/Operational' = 1
'Microsoft-Windows-NetworkSecurity/Debug' = 1
'Microsoft-Windows-NetworkStatus/Analytic' = 1
'Microsoft-Windows-Networking-Correlation/Diagnostic' = 1
'Microsoft-Windows-Networking-RealTimeCommunication/Tracing' = 1
'Microsoft-Windows-NlaSvc/Diagnostic' = 1
'Microsoft-Windows-NlaSvc/Operational' = 1
'Microsoft-Windows-Ntfs/Operational' = 1
'Microsoft-Windows-Ntfs/Performance' = 1
'Microsoft-Windows-Ntfs/WHC' = 1
'Microsoft-Windows-OLE/Clipboard-Performance' = 1
'Microsoft-Windows-OLEACC/Debug' = 1
'Microsoft-Windows-OLEACC/Diagnostic' = 1
'Microsoft-Windows-OOBE-FirstLogonAnim/Diagnostic' = 1
'Microsoft-Windows-OOBE-Machine-Core/Diagnostic' = 1
'Microsoft-Windows-OOBE-Machine-DUI/Diagnostic' = 1
'Microsoft-Windows-OOBE-Machine-DUI/Operational' = 1
'Microsoft-Windows-OOBE-Machine-Plugins-Wireless/Diagnostic' = 1
'Microsoft-Windows-OfflineFiles/Analytic' = 1
'Microsoft-Windows-OfflineFiles/Debug' = 1
'Microsoft-Windows-OfflineFiles/Operational' = 1
'Microsoft-Windows-OfflineFiles/SyncLog' = 1
'Microsoft-Windows-OneBackup/Debug' = 1
'Microsoft-Windows-OneX/Diagnostic' = 1
'Microsoft-Windows-OneX/Operational' = 1
'Microsoft-Windows-OobeLdr/Analytic' = 1
'Microsoft-Windows-OtpCredentialProvider/Operational' = 1
'Microsoft-Windows-PCI/Diagnostic' = 1
'Microsoft-Windows-PackageStateRoaming/Analytic' = 1
'Microsoft-Windows-PackageStateRoaming/Debug' = 1
'Microsoft-Windows-PackageStateRoaming/Operational' = 1
'Microsoft-Windows-ParentalControls/Operational' = 1
'Microsoft-Windows-Partition/Analytic' = 1
'Microsoft-Windows-Partition/Diagnostic' = 1
'Microsoft-Windows-PeerToPeerDrtEventProvider/Diagnostic' = 1
'Microsoft-Windows-PerceptionRuntime/Operational' = 1
'Microsoft-Windows-PerceptionSensorDataService/Operational' = 1
'Microsoft-Windows-PersistentMemory-Nvdimm/Analytic' = 1
'Microsoft-Windows-PersistentMemory-Nvdimm/Diagnostic' = 1
'Microsoft-Windows-PersistentMemory-Nvdimm/Operational' = 1
'Microsoft-Windows-PersistentMemory-PmemDisk/Analytic' = 1
'Microsoft-Windows-PersistentMemory-PmemDisk/Diagnostic' = 1
'Microsoft-Windows-PersistentMemory-PmemDisk/Operational' = 1
'Microsoft-Windows-PersistentMemory-ScmBus/Analytic' = 1
'Microsoft-Windows-PersistentMemory-ScmBus/Certification' = 1
'Microsoft-Windows-PersistentMemory-ScmBus/Diagnose' = 1
'Microsoft-Windows-PersistentMemory-ScmBus/Operational' = 1
'Microsoft-Windows-PhotoAcq/Analytic' = 1
'Microsoft-Windows-PlayToManager/Analytic' = 1
'Microsoft-Windows-Policy/Analytic' = 1
'Microsoft-Windows-Policy/Operational' = 1
'Microsoft-Windows-PortableDeviceSyncProvider/Analytic' = 1
'Microsoft-Windows-Power-Meter-Polling/Diagnostic' = 1
'Microsoft-Windows-PowerCfg/Diagnostic' = 1
'Microsoft-Windows-PowerCpl/Diagnostic' = 1
'Microsoft-Windows-PowerEfficiencyDiagnostics/Diagnostic' = 1
'Microsoft-Windows-PowerShell-DesiredStateConfiguration-FileDownloadManager/Analytic' = 1
'Microsoft-Windows-PowerShell-DesiredStateConfiguration-FileDownloadManager/Debug' = 1
'Microsoft-Windows-PowerShell-DesiredStateConfiguration-FileDownloadManager/Operational' = 1
'Microsoft-Windows-PowerShell/Admin' = 1
'Microsoft-Windows-PowerShell/Analytic' = 1
'Microsoft-Windows-PowerShell/Debug' = 1
'Microsoft-Windows-PrimaryNetworkIcon/Performance' = 1
'Microsoft-Windows-PrintBRM/Admin' = 1
'Microsoft-Windows-PrintService-USBMon/Debug' = 1
'Microsoft-Windows-PrintService/Admin' = 1
'Microsoft-Windows-PrintService/Debug' = 1
'Microsoft-Windows-PrintService/Operational' = 1
'Microsoft-Windows-ProcessStateManager/Diagnostic' = 1
'Microsoft-Windows-Program-Compatibility-Assistant/Analytic' = 1
'Microsoft-Windows-Program-Compatibility-Assistant/CompatAfterUpgrade' = 1
'Microsoft-Windows-Provisioning-Diagnostics-Provider/Admin' = 1
'Microsoft-Windows-Provisioning-Diagnostics-Provider/AutoPilot' = 1
'Microsoft-Windows-Provisioning-Diagnostics-Provider/Debug' = 1
'Microsoft-Windows-Provisioning-Diagnostics-Provider/ManagementService' = 1
'Microsoft-Windows-Proximity-Common/Diagnostic' = 1
'Microsoft-Windows-Proximity-Common/Informational' = 1
'Microsoft-Windows-Proximity-Common/Performance' = 1
'Microsoft-Windows-PushNotification-Developer/Debug' = 1
'Microsoft-Windows-PushNotification-InProc/Debug' = 1
'Microsoft-Windows-PushNotification-Platform/Admin' = 1
'Microsoft-Windows-PushNotification-Platform/Debug' = 1
'Microsoft-Windows-PushNotification-Platform/Operational' = 1
'Microsoft-Windows-QoS-Pacer/Diagnostic' = 1
'Microsoft-Windows-QoS-qWAVE/Debug' = 1
'Microsoft-Windows-RPC-Proxy/Debug' = 1
'Microsoft-Windows-RPC/Debug' = 1
'Microsoft-Windows-RPC/EEInfo' = 1
'Microsoft-Windows-RRAS/Debug' = 1
'Microsoft-Windows-RRAS/Operational' = 1
'Microsoft-Windows-RadioManager/Analytic' = 1
'Microsoft-Windows-Ras-NdisWanPacketCapture/Diagnostic' = 1
'Microsoft-Windows-RasAgileVpn/Debug' = 1
'Microsoft-Windows-RasAgileVpn/Operational' = 1
'Microsoft-Windows-ReFS/Operational' = 1
'Microsoft-Windows-ReadyBoost/Analytic' = 1
'Microsoft-Windows-ReadyBoost/Operational' = 1
'Microsoft-Windows-ReadyBoostDriver/Analytic' = 1
'Microsoft-Windows-ReadyBoostDriver/Operational' = 1
'Microsoft-Windows-Regsvr32/Operational' = 1
'Microsoft-Windows-RemoteApp and Desktop Connections/Admin' = 1
'Microsoft-Windows-RemoteApp and Desktop Connections/Operational' = 1
'Microsoft-Windows-RemoteAssistance/Admin' = 1
'Microsoft-Windows-RemoteAssistance/Operational' = 1
'Microsoft-Windows-RemoteAssistance/Tracing' = 1
'Microsoft-Windows-RemoteDesktopServices-RdpCoreTS/Admin' = 1
'Microsoft-Windows-RemoteDesktopServices-RdpCoreTS/Debug' = 1
'Microsoft-Windows-RemoteDesktopServices-RdpCoreTS/Operational' = 1
'Microsoft-Windows-RemoteDesktopServices-RemoteFX-Synth3dvsc/Admin' = 1
'Microsoft-Windows-RemoteDesktopServices-RemoteFX-VM-Kernel-Mode-Transport/Debug' = 1
'Microsoft-Windows-RemoteDesktopServices-RemoteFX-VM-User-Mode-Transport/Debug' = 1
'Microsoft-Windows-RemoteDesktopServices-SessionServices/Operational' = 1
'Microsoft-Windows-Remotefs-Rdbss/Diagnostic' = 1
'Microsoft-Windows-Remotefs-Rdbss/Operational' = 1
'Microsoft-Windows-ResetEng-Trace/Diagnostic' = 1
'Microsoft-Windows-Resource-Exhaustion-Detector/Operational' = 1
'Microsoft-Windows-Resource-Exhaustion-Resolver/Operational' = 1
'Microsoft-Windows-ResourcePublication/Tracing' = 1
'Microsoft-Windows-RestartManager/Operational' = 1
'Microsoft-Windows-RetailDemo/Admin' = 1
'Microsoft-Windows-RetailDemo/Operational' = 1
'Microsoft-Windows-Runtime-Graphics/Analytic' = 1
'Microsoft-Windows-Runtime-Networking-BackgroundTransfer/Tracing' = 1
'Microsoft-Windows-Runtime-Networking/Tracing' = 1
'Microsoft-Windows-Runtime-Web-Http/Tracing' = 1
'Microsoft-Windows-Runtime-WebAPI/Tracing' = 1
'Microsoft-Windows-Runtime/CreateInstance' = 1
'Microsoft-Windows-Runtime/Error' = 1
'Microsoft-Windows-SENSE/Operational' = 1
'Microsoft-Windows-SMBClient/Analytic' = 1
'Microsoft-Windows-SMBClient/HelperClassDiagnostic' = 1
'Microsoft-Windows-SMBClient/ObjectStateDiagnostic' = 1
'Microsoft-Windows-SMBClient/Operational' = 1
'Microsoft-Windows-SMBDirect/Admin' = 1
'Microsoft-Windows-SMBDirect/Debug' = 1
'Microsoft-Windows-SMBDirect/Netmon' = 1
'Microsoft-Windows-SMBServer/Analytic' = 1
'Microsoft-Windows-SMBServer/Connectivity' = 1
'Microsoft-Windows-SMBServer/Diagnostic' = 1
'Microsoft-Windows-SMBServer/Operational' = 1
'Microsoft-Windows-SMBServer/Performance' = 1
'Microsoft-Windows-SMBServer/Security' = 1
'Microsoft-Windows-SMBWitnessClient/Admin' = 1
'Microsoft-Windows-SMBWitnessClient/Informational' = 1
'Microsoft-Windows-SPB-ClassExtension/Analytic' = 1
'Microsoft-Windows-SPB-HIDI2C/Analytic' = 1
'Microsoft-Windows-Schannel-Events/Perf' = 1
'Microsoft-Windows-Sdbus/Analytic' = 1
'Microsoft-Windows-Sdbus/Debug' = 1
'Microsoft-Windows-Sdstor/Analytic' = 1
'Microsoft-Windows-Search-Core/Diagnostic' = 1
'Microsoft-Windows-Search-ProtocolHandlers/Diagnostic' = 1
'Microsoft-Windows-SearchUI/Diagnostic' = 1
'Microsoft-Windows-SearchUI/Operational' = 1
'Microsoft-Windows-SecureAssessment/Operational' = 1
'Microsoft-Windows-Security-Adminless/Operational' = 1
'Microsoft-Windows-Security-Audit-Configuration-Client/Diagnostic' = 1
'Microsoft-Windows-Security-Audit-Configuration-Client/Operational' = 1
'Microsoft-Windows-Security-EnterpriseData-FileRevocationManager/Operational' = 1
'Microsoft-Windows-Security-ExchangeActiveSyncProvisioning/Operational' = 1
'Microsoft-Windows-Security-ExchangeActiveSyncProvisioning/Performance' = 1
'Microsoft-Windows-Security-IdentityListener/Operational' = 1
'Microsoft-Windows-Security-IdentityStore/Performance' = 1
'Microsoft-Windows-Security-LessPrivilegedAppContainer/Operational' = 1
'Microsoft-Windows-Security-Mitigations/KernelMode' = 1
'Microsoft-Windows-Security-Mitigations/UserMode' = 1
'Microsoft-Windows-Security-Netlogon/Operational' = 1
'Microsoft-Windows-Security-SPP-UX-GC/Analytic' = 1
'Microsoft-Windows-Security-SPP-UX-GenuineCenter-Logging/Operational' = 1
'Microsoft-Windows-Security-SPP-UX-Notifications/ActionCenter' = 1
'Microsoft-Windows-Security-SPP-UX/Analytic' = 1
'Microsoft-Windows-Security-SPP/Perf' = 1
'Microsoft-Windows-Security-UserConsentVerifier/Audit' = 1
'Microsoft-Windows-Security-Vault/Performance' = 1
'Microsoft-Windows-SecurityMitigationsBroker/Admin' = 1
'Microsoft-Windows-SecurityMitigationsBroker/Operational' = 1
'Microsoft-Windows-SecurityMitigationsBroker/Perf' = 1
'Microsoft-Windows-SendTo/Diagnostic' = 1
'Microsoft-Windows-Sens/Debug' = 1
'Microsoft-Windows-SenseIR/Operational' = 1
'Microsoft-Windows-Sensors/Debug' = 1
'Microsoft-Windows-Sensors/Performance' = 1
'Microsoft-Windows-Serial-ClassExtension-V2/Analytic' = 1
'Microsoft-Windows-Serial-ClassExtension/Analytic' = 1
'Microsoft-Windows-ServiceReportingApi/Debug' = 1
'Microsoft-Windows-Services-Svchost/Diagnostic' = 1
'Microsoft-Windows-Services/Diagnostic' = 1
'Microsoft-Windows-ServicesForNFS-Client/IdentityMapping' = 1
'Microsoft-Windows-ServicesForNFS-Client/Operational' = 1
'Microsoft-Windows-ServicesForNFS-ONCRPC/Analytic' = 1
'Microsoft-Windows-Servicing/Debug' = 1
'Microsoft-Windows-SettingSync-Azure/Debug' = 1
'Microsoft-Windows-SettingSync-Azure/Operational' = 1
'Microsoft-Windows-SettingSync-OneDrive/Analytic' = 1
'Microsoft-Windows-SettingSync-OneDrive/Debug' = 1
'Microsoft-Windows-SettingSync-OneDrive/Operational' = 1
'Microsoft-Windows-SettingSync/Analytic' = 1
'Microsoft-Windows-SettingSync/Debug' = 1
'Microsoft-Windows-SettingSync/Operational' = 1
'Microsoft-Windows-SettingSync/VerboseDebug' = 1
'Microsoft-Windows-Setup/Analytic' = 1
'Microsoft-Windows-SetupCl/Analytic' = 1
'Microsoft-Windows-SetupPlatform/Analytic' = 1
'Microsoft-Windows-SetupQueue/Analytic' = 1
'Microsoft-Windows-SetupUGC/Analytic' = 1
'Microsoft-Windows-ShareMedia-ControlPanel/Diagnostic' = 1
'Microsoft-Windows-Shell-AppWizCpl/Diagnostic' = 1
'Microsoft-Windows-Shell-AuthUI-BootAnim/Diagnostic' = 1
'Microsoft-Windows-Shell-AuthUI-Common/Diagnostic' = 1
'Microsoft-Windows-Shell-AuthUI-CredUI/Diagnostic' = 1
'Microsoft-Windows-Shell-AuthUI-CredentialProviderUser/Diagnostic' = 1
'Microsoft-Windows-Shell-AuthUI-Logon/Diagnostic' = 1
'Microsoft-Windows-Shell-AuthUI-LogonUI/Diagnostic' = 1
'Microsoft-Windows-Shell-AuthUI-Shutdown/Diagnostic' = 1
'Microsoft-Windows-Shell-ConnectedAccountState/ActionCenter' = 1
'Microsoft-Windows-Shell-Core/ActionCenter' = 1
'Microsoft-Windows-Shell-Core/AppDefaults' = 1
'Microsoft-Windows-Shell-Core/Diagnostic' = 1
'Microsoft-Windows-Shell-Core/LogonTasksChannel' = 1
'Microsoft-Windows-Shell-Core/Operational' = 1
'Microsoft-Windows-Shell-DefaultPrograms/Diagnostic' = 1
'Microsoft-Windows-Shell-LockScreenContent/Diagnostic' = 1
'Microsoft-Windows-Shell-OpenWith/Diagnostic' = 1
'Microsoft-Windows-Shell-Search-UriHandler' = 1
'Microsoft-Windows-Shell-Shwebsvc' = 1
'Microsoft-Windows-Shell-ZipFolder/Diagnostic' = 1
'Microsoft-Windows-ShellCommon-StartLayoutPopulation/Diagnostic' = 1
'Microsoft-Windows-ShellCommon-StartLayoutPopulation/Operational' = 1
'Microsoft-Windows-Shsvcs/Diagnostic' = 1
'Microsoft-Windows-SleepStudy/Diagnostic' = 1
'Microsoft-Windows-SmartCard-Audit/Authentication' = 1
'Microsoft-Windows-SmartCard-DeviceEnum/Operational' = 1
'Microsoft-Windows-SmartCard-TPM-VCard-Module/Admin' = 1
'Microsoft-Windows-SmartCard-TPM-VCard-Module/Operational' = 1
'Microsoft-Windows-SmartScreen/Debug' = 1
'Microsoft-Windows-SmbClient/Audit' = 1
'Microsoft-Windows-SmbClient/Connectivity' = 1
'Microsoft-Windows-SmbClient/Diagnostic' = 1
'Microsoft-Windows-SmbClient/Security' = 1
'Microsoft-Windows-Speech-UserExperience/Diagnostic' = 1
'Microsoft-Windows-Spell-Checking/Analytic' = 1
'Microsoft-Windows-SpellChecker/Analytic' = 1
'Microsoft-Windows-Spellchecking-Host/Analytic' = 1
'Microsoft-Windows-SruMon/Diagnostic' = 1
'Microsoft-Windows-SrumTelemetry' = 1
'Microsoft-Windows-StateRepository/Debug' = 1
'Microsoft-Windows-StateRepository/Diagnostic' = 1
'Microsoft-Windows-StateRepository/Operational' = 1
'Microsoft-Windows-StateRepository/Restricted' = 1
'Microsoft-Windows-StorDiag/Operational' = 1
'Microsoft-Windows-StorPort/Operational' = 1
'Microsoft-Windows-Storage-ATAPort/Admin' = 1
'Microsoft-Windows-Storage-ATAPort/Analytic' = 1
'Microsoft-Windows-Storage-ATAPort/Debug' = 1
'Microsoft-Windows-Storage-ATAPort/Diagnose' = 1
'Microsoft-Windows-Storage-ATAPort/Operational' = 1
'Microsoft-Windows-Storage-ClassPnP/Admin' = 1
'Microsoft-Windows-Storage-ClassPnP/Analytic' = 1
'Microsoft-Windows-Storage-ClassPnP/Debug' = 1
'Microsoft-Windows-Storage-ClassPnP/Diagnose' = 1
'Microsoft-Windows-Storage-ClassPnP/Operational' = 1
'Microsoft-Windows-Storage-Disk/Admin' = 1
'Microsoft-Windows-Storage-Disk/Analytic' = 1
'Microsoft-Windows-Storage-Disk/Debug' = 1
'Microsoft-Windows-Storage-Disk/Diagnose' = 1
'Microsoft-Windows-Storage-Disk/Operational' = 1
'Microsoft-Windows-Storage-Storport/Admin' = 1
'Microsoft-Windows-Storage-Storport/Analytic' = 1
'Microsoft-Windows-Storage-Storport/Debug' = 1
'Microsoft-Windows-Storage-Storport/Diagnose' = 1
'Microsoft-Windows-Storage-Storport/Health' = 1
'Microsoft-Windows-Storage-Storport/Operational' = 1
'Microsoft-Windows-Storage-Tiering-IoHeat/Heat' = 1
'Microsoft-Windows-Storage-Tiering/Admin' = 1
'Microsoft-Windows-StorageManagement/Debug' = 1
'Microsoft-Windows-StorageManagement/Operational' = 1
'Microsoft-Windows-StorageSpaces-Driver/Diagnostic' = 1
'Microsoft-Windows-StorageSpaces-Driver/Operational' = 1
'Microsoft-Windows-StorageSpaces-Driver/Performance' = 1
'Microsoft-Windows-StorageSpaces-ManagementAgent/WHC' = 1
'Microsoft-Windows-StorageSpaces-SpaceManager/Diagnostic' = 1
'Microsoft-Windows-StorageSpaces-SpaceManager/Operational' = 1
'Microsoft-Windows-Store/Operational' = 1
'Microsoft-Windows-Storsvc/Diagnostic' = 1
'Microsoft-Windows-Subsys-Csr/Operational' = 1
'Microsoft-Windows-Subsys-SMSS/Operational' = 1
'Microsoft-Windows-Superfetch/Main' = 1
'Microsoft-Windows-Superfetch/PfApLog' = 1
'Microsoft-Windows-Superfetch/StoreLog' = 1
'Microsoft-Windows-Sysprep/Analytic' = 1
'Microsoft-Windows-System-Profile-HardwareId/Diagnostic' = 1
'Microsoft-Windows-SystemSettingsHandlers/Debug' = 1
'Microsoft-Windows-SystemSettingsThreshold/Debug' = 1
'Microsoft-Windows-SystemSettingsThreshold/Diagnostic' = 1
'Microsoft-Windows-SystemSettingsThreshold/Operational' = 1
'Microsoft-Windows-TCPIP/Diagnostic' = 1
'Microsoft-Windows-TCPIP/Operational' = 1
'Microsoft-Windows-TSF-msctf/Debug' = 1
'Microsoft-Windows-TSF-msctf/Diagnostic' = 1
'Microsoft-Windows-TSF-msutb/Debug' = 1
'Microsoft-Windows-TSF-msutb/Diagnostic' = 1
'Microsoft-Windows-TTS/Diagnostic' = 1
'Microsoft-Windows-TWinAPI/Diagnostic' = 1
'Microsoft-Windows-TWinUI/Diagnostic' = 1
'Microsoft-Windows-TWinUI/Operational' = 1
'Microsoft-Windows-TZSync/Analytic' = 1
'Microsoft-Windows-TZSync/Operational' = 1
'Microsoft-Windows-TZUtil/Operational' = 1
'Microsoft-Windows-TaskScheduler/Debug' = 1
'Microsoft-Windows-TaskScheduler/Diagnostic' = 1
'Microsoft-Windows-TaskScheduler/Maintenance' = 1
'Microsoft-Windows-TaskbarCPL/Diagnostic' = 1
'Microsoft-Windows-TerminalServices-ClientUSBDevices/Admin' = 1
'Microsoft-Windows-TerminalServices-ClientUSBDevices/Analytic' = 1
'Microsoft-Windows-TerminalServices-ClientUSBDevices/Debug' = 1
'Microsoft-Windows-TerminalServices-ClientUSBDevices/Operational' = 1
'Microsoft-Windows-TerminalServices-LocalSessionManager/Admin' = 1
'Microsoft-Windows-TerminalServices-LocalSessionManager/Analytic' = 1
'Microsoft-Windows-TerminalServices-LocalSessionManager/Debug' = 1
'Microsoft-Windows-TerminalServices-LocalSessionManager/Operational' = 1
'Microsoft-Windows-TerminalServices-MediaRedirection/Analytic' = 1
'Microsoft-Windows-TerminalServices-PnPDevices/Admin' = 1
'Microsoft-Windows-TerminalServices-PnPDevices/Analytic' = 1
'Microsoft-Windows-TerminalServices-PnPDevices/Debug' = 1
'Microsoft-Windows-TerminalServices-PnPDevices/Operational' = 1
'Microsoft-Windows-TerminalServices-Printers/Admin' = 1
'Microsoft-Windows-TerminalServices-Printers/Analytic' = 1
'Microsoft-Windows-TerminalServices-Printers/Debug' = 1
'Microsoft-Windows-TerminalServices-Printers/Operational' = 1
'Microsoft-Windows-TerminalServices-RDPClient/Analytic' = 1
'Microsoft-Windows-TerminalServices-RDPClient/Debug' = 1
'Microsoft-Windows-TerminalServices-RDPClient/Operational' = 1
'Microsoft-Windows-TerminalServices-RdpSoundDriver/Capture' = 1
'Microsoft-Windows-TerminalServices-RdpSoundDriver/Playback' = 1
'Microsoft-Windows-TerminalServices-RemoteConnectionManager/Admin' = 1
'Microsoft-Windows-TerminalServices-RemoteConnectionManager/Analytic' = 1
'Microsoft-Windows-TerminalServices-RemoteConnectionManager/Debug' = 1
'Microsoft-Windows-TerminalServices-RemoteConnectionManager/Operational' = 1
'Microsoft-Windows-TerminalServices-ServerUSBDevices/Admin' = 1
'Microsoft-Windows-TerminalServices-ServerUSBDevices/Analytic' = 1
'Microsoft-Windows-TerminalServices-ServerUSBDevices/Debug' = 1
'Microsoft-Windows-TerminalServices-ServerUSBDevices/Operational' = 1
'Microsoft-Windows-Tethering-Manager/Analytic' = 1
'Microsoft-Windows-Tethering-Station/Analytic' = 1
'Microsoft-Windows-ThemeCPL/Diagnostic' = 1
'Microsoft-Windows-ThemeUI/Diagnostic' = 1
'Microsoft-Windows-Threat-Intelligence/Analytic' = 1
'Microsoft-Windows-Time-Service-PTP-Provider/PTP-Operational' = 1
'Microsoft-Windows-Time-Service/Operational' = 1
'Microsoft-Windows-Troubleshooting-Recommended/Admin' = 1
'Microsoft-Windows-Troubleshooting-Recommended/Operational' = 1
'Microsoft-Windows-TunnelDriver' = 1
'Microsoft-Windows-UAC-FileVirtualization/Operational' = 1
'Microsoft-Windows-UAC/Operational' = 1
'Microsoft-Windows-UI-Shell/Diagnostic' = 1
'Microsoft-Windows-UIAnimation/Diagnostic' = 1
'Microsoft-Windows-UIAutomationCore/Debug' = 1
'Microsoft-Windows-UIAutomationCore/Diagnostic' = 1
'Microsoft-Windows-UIAutomationCore/Perf' = 1
'Microsoft-Windows-UIRibbon/Diagnostic' = 1
'Microsoft-Windows-USB-MAUSBHOST-Analytic' = 1
'Microsoft-Windows-USB-UCX-Analytic' = 1
'Microsoft-Windows-USB-USBHUB/Diagnostic' = 1
'Microsoft-Windows-USB-USBHUB3-Analytic' = 1
'Microsoft-Windows-USB-USBPORT/Diagnostic' = 1
'Microsoft-Windows-USB-USBXHCI-Analytic' = 1
'Microsoft-Windows-USB-USBXHCI-Trustlet-Analytic' = 1
'Microsoft-Windows-USBVideo/Analytic' = 1
'Microsoft-Windows-UniversalTelemetryClient/Operational' = 1
'Microsoft-Windows-User Control Panel Performance/Diagnostic' = 1
'Microsoft-Windows-User Control Panel Usage/Diagnostic' = 1
'Microsoft-Windows-User Control Panel/Diagnostic' = 1
'Microsoft-Windows-User Control Panel/Operational' = 1
'Microsoft-Windows-User Device Registration/Admin' = 1
'Microsoft-Windows-User Device Registration/Debug' = 1
'Microsoft-Windows-User Profile Service/Diagnostic' = 1
'Microsoft-Windows-User Profile Service/Operational' = 1
'Microsoft-Windows-User-Loader/Analytic' = 1
'Microsoft-Windows-User-Loader/Operational' = 1
'Microsoft-Windows-UserAccountControl/Diagnostic' = 1
'Microsoft-Windows-UserModePowerService/Diagnostic' = 1
'Microsoft-Windows-UserPnp/ActionCenter' = 1
'Microsoft-Windows-UserPnp/DeviceInstall' = 1
'Microsoft-Windows-UserPnp/DeviceMetadata/Debug' = 1
'Microsoft-Windows-UserPnp/Performance' = 1
'Microsoft-Windows-UserPnp/SchedulerOperations' = 1
'Microsoft-Windows-UxInit/Diagnostic' = 1
'Microsoft-Windows-UxTheme/Diagnostic' = 1
'Microsoft-Windows-VAN/Diagnostic' = 1
'Microsoft-Windows-VDRVROOT/Operational' = 1
'Microsoft-Windows-VHDMP-Analytic' = 1
'Microsoft-Windows-VHDMP-Operational' = 1
'Microsoft-Windows-VIRTDISK-Analytic' = 1
'Microsoft-Windows-VPN-Client/Operational' = 1
'Microsoft-Windows-VPN/Operational' = 1
'Microsoft-Windows-VWiFi/Diagnostic' = 1
'Microsoft-Windows-VerifyHardwareSecurity/Admin' = 1
'Microsoft-Windows-VerifyHardwareSecurity/Operational' = 1
'Microsoft-Windows-Volume/Diagnostic' = 1
'Microsoft-Windows-VolumeControl/Performance' = 1
'Microsoft-Windows-VolumeSnapshot-Driver/Analytic' = 1
'Microsoft-Windows-VolumeSnapshot-Driver/Operational' = 1
'Microsoft-Windows-WABSyncProvider/Analytic' = 1
'Microsoft-Windows-WCN-Config-Registrar/Diagnostic' = 1
'Microsoft-Windows-WCNWiz/Analytic' = 1
'Microsoft-Windows-WDAG-PolicyEvaluator-CSP/Operational' = 1
'Microsoft-Windows-WDAG-PolicyEvaluator-GP/Operational' = 1
'Microsoft-Windows-WEPHOSTSVC/Operational' = 1
'Microsoft-Windows-WER-PayloadHealth/Operational' = 1
'Microsoft-Windows-WFP/Analytic' = 1
'Microsoft-Windows-WFP/Operational' = 1
'Microsoft-Windows-WLAN-AutoConfig/Operational' = 1
'Microsoft-Windows-WLAN-Autoconfig/Diagnostic' = 1
'Microsoft-Windows-WLAN-Driver/Analytic' = 1
'Microsoft-Windows-WLAN-MediaManager/Diagnostic' = 1
'Microsoft-Windows-WLANConnectionFlow/Diagnostic' = 1
'Microsoft-Windows-WMI-Activity/Debug' = 1
'Microsoft-Windows-WMI-Activity/Trace' = 1
'Microsoft-Windows-WSC-SRV/Diagnostic' = 1
'Microsoft-Windows-WUSA/Debug' = 1
'Microsoft-Windows-WWAN-CFE/Diagnostic' = 1
'Microsoft-Windows-WWAN-MM-Events/Diagnostic' = 1
'Microsoft-Windows-WWAN-MediaManager/Diagnostic' = 1
'Microsoft-Windows-WWAN-NDISUIO-EVENTS/Diagnostic' = 1
'Microsoft-Windows-WWAN-SVC-Events/Diagnostic' = 1
'Microsoft-Windows-WWAN-SVC-Events/Operational' = 1
'Microsoft-Windows-Wcmsvc/Diagnostic' = 1
'Microsoft-Windows-Wcmsvc/Operational' = 1
'Microsoft-Windows-WebAuth/Operational' = 1
'Microsoft-Windows-WebAuthN/Operational' = 1
'Microsoft-Windows-WebIO-NDF/Diagnostic' = 1
'Microsoft-Windows-WebIO/Diagnostic' = 1
'Microsoft-Windows-WebPlatStorage-Server' = 1
'Microsoft-Windows-WebServices/Tracing' = 1
'Microsoft-Windows-Websocket-Protocol-Component/Tracing' = 1
'Microsoft-Windows-WiFiDisplay/Analytic' = 1
'Microsoft-Windows-Win32k/Concurrency' = 1
'Microsoft-Windows-Win32k/Contention' = 1
'Microsoft-Windows-Win32k/Messages' = 1
'Microsoft-Windows-Win32k/Operational' = 1
'Microsoft-Windows-Win32k/Power' = 1
'Microsoft-Windows-Win32k/Render' = 1
'Microsoft-Windows-Win32k/Tracing' = 1
'Microsoft-Windows-Win32k/UIPI' = 1
'Microsoft-Windows-WinHTTP-NDF/Diagnostic' = 1
'Microsoft-Windows-WinHttp/Diagnostic' = 1
'Microsoft-Windows-WinINet-Capture/Analytic' = 1
'Microsoft-Windows-WinINet-Config/ProxyConfigChanged' = 1
'Microsoft-Windows-WinINet/Analytic' = 1
'Microsoft-Windows-WinINet/UsageLog' = 1
'Microsoft-Windows-WinINet/WebSocket' = 1
'Microsoft-Windows-WinML/Analytic' = 1
'Microsoft-Windows-WinNat/Oper' = 1
'Microsoft-Windows-WinNat/Trace' = 1
'Microsoft-Windows-WinRM/Analytic' = 1
'Microsoft-Windows-WinRM/Debug' = 1
'Microsoft-Windows-WinURLMon/Analytic' = 1
'Microsoft-Windows-Windeploy/Analytic' = 1
'Microsoft-Windows-Windows Defender/Operational' = 1
'Microsoft-Windows-Windows Defender/WHC' = 1
'Microsoft-Windows-Windows Firewall With Advanced Security/ConnectionSecurity' = 1
'Microsoft-Windows-Windows Firewall With Advanced Security/ConnectionSecurityVerbose' = 1
'Microsoft-Windows-Windows Firewall With Advanced Security/Firewall' = 1
'Microsoft-Windows-Windows Firewall With Advanced Security/FirewallDiagnostics' = 1
'Microsoft-Windows-Windows Firewall With Advanced Security/FirewallVerbose' = 1
'Microsoft-Windows-WindowsBackup/ActionCenter' = 1
'Microsoft-Windows-WindowsColorSystem/Debug' = 1
'Microsoft-Windows-WindowsColorSystem/Operational' = 1
'Microsoft-Windows-WindowsSystemAssessmentTool/Operational' = 1
'Microsoft-Windows-WindowsSystemAssessmentTool/Tracing' = 1
'Microsoft-Windows-WindowsUIImmersive/Diagnostic' = 1
'Microsoft-Windows-WindowsUIImmersive/Operational' = 1
'Microsoft-Windows-WindowsUpdateClient/Analytic' = 1
'Microsoft-Windows-WindowsUpdateClient/Operational' = 1
'Microsoft-Windows-Wininit/Diagnostic' = 1
'Microsoft-Windows-Winlogon/Diagnostic' = 1
'Microsoft-Windows-Winlogon/Operational' = 1
'Microsoft-Windows-Winsock-AFD/Operational' = 1
'Microsoft-Windows-Winsock-NameResolution/Operational' = 1
'Microsoft-Windows-Winsock-WS2HELP/Operational' = 1
'Microsoft-Windows-Winsrv/Analytic' = 1
'Microsoft-Windows-Wired-AutoConfig/Diagnostic' = 1
'Microsoft-Windows-Wired-AutoConfig/Operational' = 1
'Microsoft-Windows-WlanDlg/Analytic' = 1
'Microsoft-Windows-Wordpad/Admin' = 1
'Microsoft-Windows-Wordpad/Debug' = 1
'Microsoft-Windows-Wordpad/Diagnostic' = 1
'Microsoft-Windows-WorkFolders/Analytic' = 1
'Microsoft-Windows-WorkFolders/Debug' = 1
'Microsoft-Windows-WorkFolders/Operational' = 1
'Microsoft-Windows-WorkFolders/WHC' = 1
'Microsoft-Windows-Workplace Join/Admin' = 1
'Microsoft-Windows-XAML-Diagnostics/Default' = 1
'Microsoft-Windows-XAML/Default' = 1
'Microsoft-Windows-XAudio2/Debug' = 1
'Microsoft-Windows-XAudio2/Performance' = 1
'Microsoft-Windows-glcnd/Admin' = 1
'Microsoft-Windows-glcnd/Debug' = 1
'Microsoft-Windows-glcnd/Diagnostic' = 1
'Microsoft-Windows-mobsync/Diagnostic' = 1
'Microsoft-Windows-ntshrui' = 1
'Microsoft-Windows-ntshrui-perf' = 1
'Microsoft-Windows-osk/Diagnostic' = 1
'Microsoft-Windows-stobject/Diagnostic' = 1
'Microsoft-WindowsPhone-Connectivity-WiFiConnSvc-Channel' = 1
'Microsoft-WindowsPhone-LocationServiceProvider/Debug' = 1
'Microsoft-WindowsPhone-Net-Cellcore-CellManager/Debug' = 1
'Microsoft-WindowsPhone-Net-Cellcore-CellularAPI/Debug' = 1
'NIS-Driver-WFP/Diagnostic' = 1
'Navigator' = 1
'Network Isolation Operational' = 1
'OSK_SoftKeyboard_Channel' = 1
'OpenSSH/Admin' = 1
'OpenSSH/Debug' = 1
'OpenSSH/Operational' = 1
'Physical_Keyboard_Manager_Channel' = 1
'PlayReadyPerformanceChannel' = 1
'RTWorkQueueExtended' = 1
'RTWorkQueueTheading' = 1
'SMSApi' = 1
'Setup' = 1
'SmbWmiAnalytic' = 1
'SystemEventsBroker' = 1
'TabletPC_InputPanel_Channel' = 1
'TabletPC_InputPanel_Channel/IHM' = 1
'TimeBroker' = 1
'UIManager_Channel' = 1
'WINDOWS_KS_CHANNEL' = 1
'WINDOWS_WMPHOTO_CHANNEL' = 1
'Windows Networking Vpn Plugin Platform/Operational' = 1
'Windows Networking Vpn Plugin Platform/OperationalVerbose' = 1
}

foreach ($l in $logs.keys){
    Try{
        $lsize = [math]::Round((Get-WinEvent -ListLog $l -ErrorAction Stop).MaximumSizeInBytes / (1024*1024*1024),3)
        if ($lsize -lt $logs[$l]){
            #$neg_str + $l "max log size is smaller than $logs[$l] GB: $lsize GB" | Tee-Object -FilePath $out_file -Append
            $neg_str + "$l max log size is smaller than $logs[$l] GB: $lsize GB" | Tee-Object -FilePath $out_file -Append
        } else {
            #$pos_str + $l "max log size is okay: $lsize GB" | Tee-Object -FilePath $out_file -Append
            $pos_str + "$l max log size is okay: $lsize GB" | Tee-Object -FilePath $out_file -Append
        }
    }
    Catch{
        $err_str + "Testing $l log size failed." | Tee-Object -FilePath $out_file -Append
    }
}

###############################

########## PowerShell Settings ##############

# Check PowerShell Version. Should be > 5. Powershell 2 should not be installed.
<#
Resources:
    Detecting Offensive PowerShell Attack Tools: https://adsecurity.org/?p=2604
    How to Enable or Disable Windows PowerShell 2.0 in Windows 10: https://www.tenforums.com/tutorials/111654-enable-disable-windows-powershell-2-0-windows-10-a.html
    Check Installed .NET Versions Using PowerShell: https://www.syspanda.com/index.php/2018/09/25/check-installed-net-versions-using-powershell/
    PowerShell Security Best Practices: https://www.digitalshadows.com/blog-and-research/powershell-security-best-practices/
#>

$inf_str + "Testing if PowerShell Version is at least version 5" | Tee-Object -FilePath $out_file -Append
Try{
    $psver = $PSVersionTable.PSVersion.Major
    $fpsver = $PSVersionTable.PSVersion
    if ([int]$psver -lt 5) { 
        $neg_str + "Current PowerShell Version is less than Version 5: $fpsver"  | Tee-Object -FilePath $out_file -Append
    } else { 
        $pos_str + "Current PowerShell Version: $fpsver" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing PowerShell Version failed." | Tee-Object -FilePath $out_file -Append
}

$inf_str + "Testing if PowerShell Version 2 is permitted" | Tee-Object -FilePath $out_file -Append
Try{
    # NOTE: Workstation test. Servers would need to test "Get-WindowsFeature PowerShell-V2"
    $psver2 = (Get-WindowsOptionalFeature -Online -FeatureName MicrosoftWindowsPowerShellV2).state
    if ($psver2 -ne $null){
        if ($psver2 -eq 'Enabled') { 
            $neg_str + "PowerShell Version 2 should be disabled: $psver2" | Tee-Object -FilePath $out_file -Append
        } else { 
            $pos_str + "PowerShell Version 2 is: $psver2" | Tee-Object -FilePath $out_file -Append
        }
    }else{
        $inf_str + "Get-WindowsOptionalFeature is not available to test if PowerShell Version 2 is permitted." | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for PowerShell Version 2 failed." | Tee-Object -FilePath $out_file -Append
}

$inf_str + "Testing if .NET Framework version supports PowerShell Version 2" | Tee-Object -FilePath $out_file -Append
Try{
    $netv=(Get-ChildItem 'HKLM:\SOFTWARE\Microsoft\NET Framework Setup\NDP' -Recurse -ErrorAction SilentlyContinue| Get-ItemProperty -Name Version -ErrorAction SilentlyContinue).Version
    foreach ($e in $netv){
        if ($e -lt 3.0) {
            $neg_str + ".NET Framework less than 3.0 installed which could allow PS2 execution: $e" | Tee-Object -FilePath $out_file -Append
        } else {
            $pos_str + ".NET Framework greater than 3.0 installed: $e" | Tee-Object -FilePath $out_file -Append
        }
    }
}
Catch{
    $err_str + "Testing for .NET vesions that support PowerShell Version 2 failed." | Tee-Object -FilePath $out_file -Append
}

# Check PowerShell Language Mode. It should be "ConstrainedLanguage"
<#
Resources:
    Detecting Offensive PowerShell Attack Tools: https://adsecurity.org/?p=2604
#>
$inf_str + "Testing if PowerShell is configured to use Constrained Language." | Tee-Object -FilePath $out_file -Append
Try{
    $ps_lang = $ExecutionContext.SessionState.LanguageMode
    if ($ps_lang -eq 'ConstrainedLanguage') { 
        $pos_str + "Execution Langugage Mode Is: $ps_lang" | Tee-Object -FilePath $out_file -Append
    } else  { 
        $neg_str + "Execution Langugage Mode Is Not ConstrainedLanguage: $ps_lang" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for PowerShell Constrained Language failed." | Tee-Object -FilePath $out_file -Append
}
###############################

########## Cached Creds ##############
<#
Resources:
    Cached logons and CachedLogonsCount: https://blogs.technet.microsoft.com/instan/2011/12/06/cached-logons-and-cachedlogonscount/
    Cached domain logon information: https://support.microsoft.com/en-us/help/172931/cached-domain-logon-information
#>

# Cached Logons should be set to 0 or 1.
$inf_str + "Testing if system is configured to limit the number of stored credentials." | Tee-Object -FilePath $out_file -Append
Try{
    $regv = (Get-ItemProperty -path "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -ErrorAction SilentlyContinue).CachedLogonsCount
    if ([int]$regv -lt 2) {
        $pos_str + "CachedLogonsCount Is Set to: $regv" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "CachedLogonsCount Is Not Set to 0 or 1: $regv" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for stored credential settings failed." | Tee-Object -FilePath $out_file -Append
}

###############################
########## Remote Access ##############

<#
Resources:
    How to disable RDP access for Administrator: https://serverfault.com/questions/598278/how-to-disable-rdp-access-for-administrator/598279
    How to Remotely Enable and Disable (RDP) Remote Desktop: https://www.interfacett.com/blogs/how-to-remotely-enable-and-disable-rdp-remote-desktop/
#>

# Deny RDP Connections
$inf_str + "Testing if system is configured to prevent RDP service." | Tee-Object -FilePath $out_file -Append
Try{
    if ([bool](Get-ItemProperty -path "HKLM:\System\CurrentControlSet\Control\Terminal Server" -ErrorAction SilentlyContinue).AllowRemoteRPC){
        $neg_str + "AllowRemoteRPC is should be set to disable RDP: 1" | Tee-Object -FilePath $out_file -Append
    } else {
        $pos_str + "AllowRemoteRPC is set to deny RDP: 0" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing if system prevents RDP service failed." | Tee-Object -FilePath $out_file -Append
}

$inf_str + "Testing if system is configured to deny remote access via Terminal Services." | Tee-Object -FilePath $out_file -Append
Try{
    if ([bool](Get-ItemProperty -path "HKLM:\System\CurrentControlSet\Control\Terminal Server" -ErrorAction SilentlyContinue).fDenyTSConnections){
        $pos_str + "fDenyTSConnections is set to deny remote connections: 1" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "fDenyTSConnections should be set to not allow remote connections: 0" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing if system denies remote access via Terminal Services failed." | Tee-Object -FilePath $out_file -Append
}

# Check if the WinRM Service is running
<#
Resources:
    NEED TO TEST IF WINRM IS LISTENING?: https://stevenmurawski.com/2015/06/need-to-test-if-winrm-is-listening/
    Enable PowerShell Remoting and check if it’s enabled: https://www.dtonias.com/enable-powershell-remoting-check-enabled/
#>

$inf_str + "Testing if WinFW Service is running." | Tee-Object -FilePath $out_file -Append
Try{
    if (Test-WSMan -ErrorAction Stop) { 
        $neg_str + "WinRM Services is running and may be accepting connections: Test-WSMan check."  | Tee-Object -FilePath $out_file -Append
    } else { 
        $pos_str + "WinRM Services is not running: Test-WSMan check."  | Tee-Object -FilePath $out_file -Append
    }   
}
Catch{
    Try{
        $ress = (Get-Service WinRM).status
        if ($ress -eq 'Stopped') { 
            $pos_str + "WinRM Services is not running: Get-Service check."  | Tee-Object -FilePath $out_file -Append
        } else { 
            $neg_str + "WinRM Services is running and may be accepting connections: Get-Service check."  | Tee-Object -FilePath $out_file -Append
        }
    }
    Catch{
        $err_str + "Testing if WimRM Service is running failed." | Tee-Object -FilePath $out_file -Append
    }
}

$inf_str + "Testing if Windows Network Firewall rules allow remote connections." | Tee-Object -FilePath $out_file -Append
Try{
    $resfw = Get-NetFirewallRule -DisplayName 'Windows Remote Management (HTTP-In)'
    foreach ($r in $resfw){
        if ($r.Enabled -eq 'False'){
            $pos_str + "WinRM Firewall Rule $r.Name is disabled." | Tee-Object -FilePath $out_file -Append
        } else {
            $neg_str + "WinRM Firewall Rule $r.Name is enabled." | Tee-Object -FilePath $out_file -Append
        }
    }
}
Catch{
    $err_str + "Testing if Windows Network Firewall rules failed." | Tee-Object -FilePath $out_file -Append
}

###############################
########## Local Administrator Accounts ##############
###############################
# BugFix 20190523: Mike Saunders - RedSiege, LLC
<#
Resources:
    Get-LocalGroupMember: https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.localaccounts/get-localgroupmember?view=powershell-5.1
#>
$inf_str + "Testing Local Administrator Accounts." | Tee-Object -FilePath $out_file -Append
Try{
    $numadmin = (Get-LocalGroupMember -Group "Administrators" -ErrorAction Stop).Name.count
    if ([int]$numadmin -gt 1){
        $neg_str + "More than one account is in local Administrators group: $numadmin" | Tee-Object -FilePath $out_file -Append
    } else {
        $pos_str + "One account in local Administrators group." | Tee-Object -FilePath $out_file -Append
    }

    foreach ($n in (Get-LocalGroupMember -Group "Administrators").Name) {
        $inf_str + "Account in local Administrator group: $n" | Tee-Object -FilePath $out_file -Append
    }
}
# No PS Cmdlet, use net command
Catch [System.InvalidOperationException]{
    $netout = (net localgroup Administrators)
    foreach ($item in $netout){
        if ($item -match '----') {
            $index = $netout.IndexOf($item)
        }
    }

    $numadmin = $netout[($index + 1)..($netout.Length - 3)]
    if ($content.length -gt 1){
        $neg_str + "More than one account is in local Administrators group: $numadmin.length" | Tee-Object -FilePath $out_file -Append
    } else {
        $pos_str + "One account in local Administrators group." | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing local Administrator Accounts failed." | Tee-Object -FilePath $out_file -Append
}

###############################
###### Secure Baseline ##############
###############################
<#
Resources:
    Securing Windows Workstations: Developing a Secure Baseline: https://adsecurity.org/?p=3299
    Microsoft Local Administrator Password Solution (LAPS): https://adsecurity.org/?p=1790
    How to Disable LLMNR & Why You Want To: https://www.blackhillsinfosec.com/how-to-disable-llmnr-why-you-want-to/
#>

###############################
# Check AppLocker
###############################

# Deploy Microsoft AppLocker to lock down what can run on the system.

$inf_str + "Testing if AppLocker is configured." | Tee-Object -FilePath $out_file -Append
Try{
    $resapp = (Get-AppLockerPolicy -local -ErrorAction Stop).RuleCollectionTypes
    if ($resapp){
        $resapp_names = $resapp -join ','
        if ($resapp.Contains('Script')){
            $pos_str + "AppLocker configured to manage PowerShell scripts: $resapp_names" | Tee-Object -FilePath $out_file -Append
        } else {

            $neg_str + "AppLocker not configured to manage PowerShell scripts: $resapp_names" | Tee-Object -FilePath $out_file -Append
        }
    } else {
        $neg_str + "AppLocker not configured" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for Microsoft AppLocker failed." | Tee-Object -FilePath $out_file -Append
}

###############################
# Check EMET.
###############################
# Deploy current version of EMET with recommended software settings.

Try{
    if ([System.Environment]::OSVersion.Version.Major -lt 10){
        $resemet = (get-service EMET_Service).status
        if ($resemet -eq 'Running'){
            $pos_str + "EMET Service is running." | Tee-Object -FilePath $out_file -Append
        } else {
            $neg_str + "EMET Service is not running: $resemet" | Tee-Object -FilePath $out_file -Append
        }
    } else {
        $inf_str + "EMET Service components are built into Windows 10." | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for Microsoft EMET service failed." | Tee-Object -FilePath $out_file -Append
}

###############################
# Deploy LAPS to manage the local Administrator (RID 500) password.
###############################
$inf_str + "Testing if Local Administrator Password Solution (LAPS) is installed." | Tee-Object -FilePath $out_file -Append
Try{
    if (Get-ChildItem ‘C:\program files\LAPS\CSE\Admpwd.dll’ -ErrorAction Stop){
        $pos_str + "Local Administrator Password Solution (LAPS) is installed." | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "Local Administrator Password Solution (LAPS) is not installed." | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for Microsoft LAPS failed." | Tee-Object -FilePath $out_file -Append
}

###############################
# GPOs configured to apply when processed, not change.
###############################
# Force Group Policy to reapply settings during “refresh”

<#
Resources:
    Group Policy objects must be reprocessed even if they have not changed.: https://www.stigviewer.com/stig/windows_server_2012_member_server/2014-01-07/finding/V-4448
#>

$inf_str + "Testing if Group Policy Objects." | Tee-Object -FilePath $out_file -Append
Try{
    $ressess = (Get-ItemProperty -path 'HKLM:\Software\Policies\Microsoft\Windows\Group Policy\{35378EAC-683F-11D2-A89A-00C04FBBCFA2}\' -ErrorAction SilentlyContinue).NoGPOListChanges
    if ($ressess -ne $null){
        if ($ressess){
            $neg_str + "GPO settings are configured to only be applied after change: $ressess" | Tee-Object -FilePath $out_file -Append
        } else {
            $pos_str + "GPO settings are configured to be applied when GPOs are processed: $ressess" | Tee-Object -FilePath $out_file -Append
        }
    } else {
        $inf_str + "System may not be assigned GPOs." | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for Microsoft GPO failed." | Tee-Object -FilePath $out_file -Append
}

###############################
# Disable Net Session Enumeration (NetCease)
###############################
# NetCease is run on a DC, not local system.
<#
Resources:
View Net Session Enum Permissions: https://gallery.technet.microsoft.com/scriptcenter/View-Net-Session-Enum-dfced139
Net Cease - Hardening Net Session Enumeration: https://gallery.technet.microsoft.com/Net-Cease-Blocking-Net-1e8dcb5b
#>

$inf_str + "Testing Net Session Enumeration configuration using the TechNet script NetSessEnumPerm.ps1" | Tee-Object -FilePath $out_file -Append

###############################
# Disable WPAD
###############################
# Disable WPAD
<#
Resources:
    Microsoft Security Bulletin MS16-063 - Critical: https://docs.microsoft.com/en-us/security-updates/SecurityBulletins/2016/ms16-063
#>

$inf_str + "Testing for WPAD entry in $env:systemdrive\Windows\System32\Drivers\etc\hosts" | Tee-Object -FilePath $out_file -Append
Try{
    $reswpad = Select-String -path $env:systemdrive\Windows\System32\Drivers\etc\hosts -pattern wpad
    if ($resllmnr -ne $null){
        $pos_str + "WPAD entry detected: $reswpad" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "No WPAD entry detected. Should contain: wpad 255.255.255.255" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for WPAD in /etc/hosts failed." | Tee-Object -FilePath $out_file -Append
}

$inf_str + "Testing for WPADOverride registry key." | Tee-Object -FilePath $out_file -Append
Try{
    $reswpad2 = (Get-ItemProperty -path 'HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Internet Settings\Wpad' -ErrorAction SilentlyContinue).WpadOverride
    if ($reswpad2 -ne $null){
        if ($reswpad2){
            $pos_str + "WpadOverride registry key is configured to disable WPAD: $reswpad2" | Tee-Object -FilePath $out_file -Append
        } else {
            $neg_str + "WpadOverride registry key is configured to allow WPAD: $reswpad2" | Tee-Object -FilePath $out_file -Append
        }
    } else {
        $inf_str + "System not configured with the WpadOverride registry key." | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for WpadOverride registry key failed." | Tee-Object -FilePath $out_file -Append
}

$inf_str + "Testing WinHttpAutoProxySvc configuration." | Tee-Object -FilePath $out_file -Append
Try{
    $reswpads = (Get-Service -name WinHttpAutoProxySvc).Status
    if ($reswpads -ne $null){
        if ($reswpads -eq 'Running'){
            $neg_str + "WinHttpAutoProxySvc service is: $reswpads" | Tee-Object -FilePath $out_file -Append
        } else {
            $pos_str + "WinHttpAutoProxySvc service is: $reswpads" | Tee-Object -FilePath $out_file -Append
        }
    } else {
        $inf_str + "WinHttpAutoProxySvc service was not found: $reswpads" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for WinHttpAutoProxySvc failed." | Tee-Object -FilePath $out_file -Append
}

# Deploy security back-port patch (KB3165191).
$inf_str + "Testing if KB3165191 is installed to harden WPAD by check installation date." | Tee-Object -FilePath $out_file -Append
Try{
    $reswphf = (Get-HotFix -id KB3165191 -ErrorAction SilentlyContinue).InstalledOn
    if ($reswphf -ne $null){
        $pos_str + "KB3165191 to harden WPAD is installed: $reswphf" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "KB3165191 to harden WPAD is not installed." | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for WPAD KB3165191 failed." | Tee-Object -FilePath $out_file -Append
}

# NetBIOS configuration tested later.

# Check WINS configuration.
$inf_str + "Testing if Network Adapters are configured to enable WINS Resolution: DNSEnabledForWINSResolution" | Tee-Object -FilePath $out_file -Append
Try{
    if ([bool](Get-WmiObject -Class Win32_NetworkAdapterConfiguration -Filter "IPEnabled=TRUE").DNSEnabledForWINSResolution){
        $neg_str + "DNSEnabledForWINSResolution is enabled" | Tee-Object -FilePath $out_file -Append
    } else {
        $pos_str + "DNSEnabledForWINSResolution is disabled" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for WINS Resolution: DNSEnabledForWINSResolution failed." | Tee-Object -FilePath $out_file -Append
}

$inf_str + "Testing if Network Adapters are configured to enable WINS Resolution: WINSEnableLMHostsLookup" | Tee-Object -FilePath $out_file -Append
Try{
    if ([bool](Get-WmiObject -Class Win32_NetworkAdapterConfiguration -Filter "IPEnabled=TRUE").WINSEnableLMHostsLookup){
        $neg_str + "WINSEnableLMHostsLookup is enabled" | Tee-Object -FilePath $out_file -Append
    } else {
        $pos_str + "WINSEnableLMHostsLookup is disabled" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for WINS Resolution: WINSEnableLMHostsLookup failed." | Tee-Object -FilePath $out_file -Append
}

###############################
# Disable LLMNR
###############################
$inf_str + "Testing if LLMNR is disabled." | Tee-Object -FilePath $out_file -Append
Try{
    $resllmnr = (Get-ItemProperty -path 'HKLM:\Software\policies\Microsoft\Windows NT\DNSClient' -ErrorAction SilentlyContinue).EnableMulticast
    if ($resllmnr -ne $null){
        $pos_str + "DNSClient.EnableMulticast is disabled: $resllmnr" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "DNSClient.EnableMulticast does not exist or is enabled: $resllmnr" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for LLMNR failed." | Tee-Object -FilePath $out_file -Append
}

###############################
# Disable Windows Browser Protocol
###############################

$inf_str + "Testing if Computer Browser service is disabled." | Tee-Object -FilePath $out_file -Append
Try{
    $resbr = (Get-Service -name Browser).Status
    if ($resbr -ne $null){
        if ($resbr -eq 'Running'){
            $neg_str + "Computer Browser service is: $resbr" | Tee-Object -FilePath $out_file -Append
        } else {
            $pos_str + "Computer Browser service is: $resbr" | Tee-Object -FilePath $out_file -Append
        }
    } else {
        $inf_str + "Computer Browser service was not found: $resbr" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for Computer Browser service failed." | Tee-Object -FilePath $out_file -Append
}

###############################
# Disable NetBIOS
###############################
<#
Resources:
    Getting TCP/IP Netbios information: https://powershell.org/forums/topic/getting-tcpip-netbios-information/
#>

$inf_str + "Testing if NetBios is disabled." | Tee-Object -FilePath $out_file -Append
Try{
    $resnetb = (Get-WmiObject -Class Win32_NetWorkAdapterConfiguration -Filter "IPEnabled=$true").TcpipNetbiosOptions
    if ($resnetb -eq $null){
        $neg_str + "NetBios TcpipNetbiosOptions key does not exist." | Tee-Object -FilePath $out_file -Append
    } else {
        if ([int]$resnetb -eq 2){
            $pos_str + "NetBios is Disabled: $resnetb" | Tee-Object -FilePath $out_file -Append
        } else {
            $neg_str + "NetBios is Enabled: $resnetb" | Tee-Object -FilePath $out_file -Append
        }
    }
}
Catch{
    $err_str + "Testing for NetBios failed." | Tee-Object -FilePath $out_file -Append
}

###############################
# Disable Windows Scripting
###############################
# Disable Windows Scripting Host (WSH) & Control Scripting File Extensions
$inf_str + "Testing if Windows Scripting Host (WSH) is disabled." | Tee-Object -FilePath $out_file -Append
Try{
    $ressh = (Get-ItemProperty -path 'HKLM:\Software\Microsoft\Windows Script Host\Settings').Enabled
    if ($ressh -eq $null){
        $neg_str + "WSH Setting Enabled key does not exist." | Tee-Object -FilePath $out_file -Append
    } else {
        if ($ressh){
            $neg_str + "WSH Setting Enabled key is Enabled: $ressh" | Tee-Object -FilePath $out_file -Append
        } else {
            $pos_str + "WSH Setting Enabled key is Disabled: $ressh" | Tee-Object -FilePath $out_file -Append
        }
    }
}
Catch{
    $err_str + "Testing for Windows Scripting Host (WSH) failed." | Tee-Object -FilePath $out_file -Append
}

# Deploy security back-port patch (KB2871997).
$inf_str +  "Testing if security back-port patch KB2871997 is installed by check installation date." | Tee-Object -FilePath $out_file -Append
Try{
    $reshf = (Get-HotFix -id KB2871997 -ErrorAction SilentlyContinue).InstalledOn
    if ($reshf -ne $null){
        $pos_str + "KB2871997 is installed: $reshf" | Tee-Object -FilePath $out_file -Append
    } else {
        $neg_str + "KB2871997 is not installed." | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for security back-port patch KB2871997 failed." | Tee-Object -FilePath $out_file -Append
}

# Not sure how to test Control Scripting File Extensions

###############################
# Prevent Interactive Login
###############################
# Prevent local Administrator (RID 500) accounts from authenticating over the network
<#
Resources:
    Disable PowerShell remoting: Disable-PSRemoting, WinRM, listener, firewall, LocalAccountTokenFilterPolicy: https://4sysops.com/wiki/disable-powershell-remoting-disable-psremoting-winrm-listener-firewall-and-localaccounttokenfilterpolicy/
    Pass-the-Hash Is Dead: Long Live LocalAccountTokenFilterPolicy: https://www.harmj0y.net/blog/redteaming/pass-the-hash-is-dead-long-live-localaccounttokenfilterpolicy/
#>

$inf_str + "Testing if PowerShell LocalAccountTokenFilterPolicy in Policies is Disabled" | Tee-Object -FilePath $out_file -Append
Try{
    if ([bool](Get-ItemProperty -path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\policies\system' -ErrorAction SilentlyContinue).LocalAccountTokenFilterPolicy){
        $neg_str + "LocalAccountTokenFilterPolicy Is Set" | Tee-Object -FilePath $out_file -Append
    } else {
        $pos_str + "LocalAccountTokenFilterPolicy Is Not Set" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for LocalAccountTokenFilterPolicy in Policies failed." | Tee-Object -FilePath $out_file -Append
}

$inf_str + "Testing if PowerShell LocalAccountTokenFilterPolicy in Wow6432Node Policies is Disabled" | Tee-Object -FilePath $out_file -Append
Try{
    if ([bool](Get-ItemProperty -path 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\policies\system' -ErrorAction SilentlyContinue).LocalAccountTokenFilterPolicy){
        $neg_str + "LocalAccountTokenFilterPolicy in Wow6432Node Is Set" | Tee-Object -FilePath $out_file -Append
    } else {
        $pos_str + "LocalAccountTokenFilterPolicy in Wow6432Node Is Not Set" | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for LocalAccountTokenFilterPolicy in Wow6432Node failed." | Tee-Object -FilePath $out_file -Append
}

###############################
# Disable WDigest
###############################
# Ensure WDigest is disabled

$inf_str + "Testing if WDigest is disabled."
Try{
    $reswd = (Get-ItemProperty -path 'HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\WDigest').UseLogonCredential
    if ($reswd -eq $null){
        $neg_str + "WDigest UseLogonCredential key does not exist." | Tee-Object -FilePath $out_file -Append
    } else {
        if ($reswd){
            $neg_str + "WDigest UseLogonCredential key is Enabled: $reswd" | Tee-Object -FilePath $out_file -Append
        } else {
            $pos_str + "WDigest UseLogonCredential key is Disabled: $reswd" | Tee-Object -FilePath $out_file -Append
        }
    }
}
Catch{
    $err_str + "Testing for WDigest failed." | Tee-Object -FilePath $out_file -Append
}

###############################
# Disable SMBV1
###############################
# Check if SMBv1 is available and if signing is turned on.
<#
Resources:
    Stop using SMB1: https://blogs.technet.microsoft.com/filecab/2016/09/16/stop-using-smb1/
    How to detect, enable and disable SMBv1, SMBv2, and SMBv3 in Windows and Windows Server: https://support.microsoft.com/en-us/help/2696547/how-to-detect-enable-and-disable-smbv1-smbv2-and-smbv3-in-windows-and
    Detecting and remediating SMBv1: https://blogs.technet.microsoft.com/leesteve/2017/05/11/detecting-and-remediating-smbv1/
#>

# Remove SMB v1 support
#$inf_str + "Testing if SMBv1 is disabled." | Tee-Object -FilePath $out_file -Append
Try{
    $ressmb = Get-SmbServerConfiguration | Select-Object EnableSMB1Protocol 
    $inf_str + "Testing if SMBv1 is disabled." | Tee-Object -FilePath $out_file -Append
    if ([bool] $ressmb) { 
        $pos_str + "SMBv1 is Disabled"  | Tee-Object -FilePath $out_file -Append
    } else { 
        $neg_str + "SMBv1 is Enabled"  | Tee-Object -FilePath $out_file -Append
    }
    $inf_str + "Testing if system is configured to audit SMBv1 activity." | Tee-Object -FilePath $out_file -Append
    if ([bool](Get-SmbServerConfiguration | Select-Object AuditSmb1Access)) { 
        $pos_str + "SMBv1 Auditing should be Enabled: Enabled"  | Tee-Object -FilePath $out_file -Append
    } else { 
        $neg_str + "SMBv1 Auditing is Disabled"  | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for SMBv1 failed." | Tee-Object -FilePath $out_file -Append
}

# Remove SMB v2 support

Try{
    $ressmb = Get-SmbServerConfiguration | Select-Object EnableSMB2Protocol 
    $inf_str + "Testing if SMBv2 is disabled." | Tee-Object -FilePath $out_file -Append
    if ([bool] $ressmb) { 
        $pos_str + "SMBv2 is Disabled"  | Tee-Object -FilePath $out_file -Append
    } else { 
        $neg_str + "SMBv2 is Enabled"  | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for SMBv2 failed." | Tee-Object -FilePath $out_file -Append
}

# Remove SMB v3 support

Try{
    $ressmb = Get-SmbServerConfiguration | Select-Object EnableSMB3Protocol 
    $inf_str + "Testing if SMBv3 is disabled." | Tee-Object -FilePath $out_file -Append
    if ([bool] $ressmb) { 
        $inf_str + "SMBv3 is Enabled"  | Tee-Object -FilePath $out_file -Append
    } else { 
        $inf_str + "SMBv3 is Disabled"  | Tee-Object -FilePath $out_file -Append
    }
    $inf_str + "Testing if system is configured to audit SMBv3 activity." | Tee-Object -FilePath $out_file -Append
    if ([bool](Get-SmbServerConfiguration | Select-Object AuditSmb2Access)) { 
        $inf_str + "SMBv3 Auditing should be Enabled: Enabled"  | Tee-Object -FilePath $out_file -Append
    } else { 
        $inf_str + "SMBv3 Auditing is Disabled"  | Tee-Object -FilePath $out_file -Append
    }
}
Catch{
    $err_str + "Testing for SMBv3 failed." | Tee-Object -FilePath $out_file -Append
}

###############################
# Untrusted Fonts
###############################
# Block Untrusted Fonts
<#
Resources:
    Block untrusted fonts in an enterprise: https://docs.microsoft.com/en-us/windows/security/threat-protection/block-untrusted-fonts-in-enterprise
    How to Verify if Device Guard is Enabled or Disabled in Windows 10: https://www.tenforums.com/tutorials/68926-verify-if-device-guard-enabled-disabled-windows-10-a.html
#>

if ([System.Environment]::OSVersion.Version.Major -eq 10){
    $inf_str + "Testing if Untrusted Fonts are disabled using the Kernel MitigationOptions." | Tee-Object -FilePath $out_file -Append
    Try{
        $resuf = (Get-ItemProperty -path 'HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\Kernel\').MitigationOptions
        if ($resuf -eq $null){
            $neg_str + "Kernel MitigationOptions key does not exist." | Tee-Object -FilePath $out_file -Append
        } else {
            if ($ressh -ge 2000000000000){
                $neg_str + "Kernel MitigationOptions key is configured not to block: $resuf" | Tee-Object -FilePath $out_file -Append
            } else {
                $pos_str + "Kernel MitigationOptions key is set to block: $resuf" | Tee-Object -FilePath $out_file -Append
            }
        }
    }
    Catch{
        $err_str + "Testing for Untrusted Fonts configuration failed." | Tee-Object -FilePath $out_file -Append
    }
} else {
    $inf_str + "Windows Version is not 10. Cannot test for Untrusted Fonts." | Tee-Object -FilePath $out_file -Append
}

###############################
# Credential and Device Guard
###############################
# Enable Credential Guard
# Configure Device Guard
<#
Resources:
    Block untrusted fonts in an enterprise: https://docs.microsoft.com/en-us/windows/security/threat-protection/block-untrusted-fonts-in-enterprise
    How to Verify if Device Guard is Enabled or Disabled in Windows 10: https://www.tenforums.com/tutorials/68926-verify-if-device-guard-enabled-disabled-windows-10-a.html
    Security Focus: Check Credential Guard Status with PowerShell: https://blogs.technet.microsoft.com/poshchap/2016/09/23/security-focus-check-credential-guard-status-with-powershell/
#>

if ([System.Environment]::OSVersion.Version.Major -eq 10){
    $inf_str + "Testing for Credential Guard." | Tee-Object -FilePath $out_file -Append
    Try{
        if ((Get-CimInstance –ClassName Win32_DeviceGuard –Namespace root\Microsoft\Windows\DeviceGuard).SecurityServicesRunning){
            $pos_str + "Credential Guard or HVCI service is running." | Tee-Object -FilePath $out_file -Append
        } else {
            $neg_str + "Credential Guard or HVCI service is not running." | Tee-Object -FilePath $out_file -Append
        }
    }
    Catch{
        $err_str + "Testing for Credential Guard failed." | Tee-Object -FilePath $out_file -Append
    }

    $inf_str + "Testing for Device Guard." | Tee-Object -FilePath $out_file -Append
    Try{
        if ((Get-CimInstance –ClassName Win32_DeviceGuard –Namespace root\Microsoft\Windows\DeviceGuard).AvailableSecurityProperties){
            $pos_str + "Device Guard appears to be configured." | Tee-Object -FilePath $out_file -Append
        } else {
            $neg_str + "Device Guard, no properties exist and therefore is not configured." | Tee-Object -FilePath $out_file -Append
        }
    }
    Catch{
        $err_str + "Testing for Device Guard failed." | Tee-Object -FilePath $out_file -Append
    }

} else {
    $inf_str + "Windows Version is not 10. Cannot test for Credential or Device Guard." | Tee-Object -FilePath $out_file -Append
}

###############################
# Secure LanMan
###############################

#Configure Lanman Authentication to a secure setting

<#
Resources:
    Understanding the Anonymous Enumeration Policies:https://www.itprotoday.com/compute-engines/understanding-anonymous-enumeration-policies
    The LanMan authentication level must be set to send NTLMv2 response only, and to refuse LM and NTLM.: https://www.stigviewer.com/stig/windows_8/2014-01-07/finding/V-1153
#>

$inf_str + "Testing Lanman Authentication for NoLmHash registry key." | Tee-Object -FilePath $out_file -Append
Try{
    $reslm = (Get-ItemProperty -path 'HKLM:\System\CurrentControlSet\Control\Lsa\').NoLmHash
    if ($reslm -eq $null){
        $neg_str + "NoLmHash registry key is not configured." | Tee-Object -FilePath $out_file -Append
    } else {
        if ($reslm){
            $pos_str + "NoLmHash registry key is configured: $reslm" | Tee-Object -FilePath $out_file -Append
        } else {        
            $neg_str + "NoLmHash registry key is not configured: $reslm" | Tee-Object -FilePath $out_file -Append
        }   
    }
}
Catch{
    $err_str + "Testing for NoLmHash registry key failed." | Tee-Object -FilePath $out_file -Append
}

$inf_str + "Testing Lanman Authentication for LM Compatability Level registry key." | Tee-Object -FilePath $out_file -Append
Try{
    $reslmcl = (Get-ItemProperty -path 'HKLM:\System\CurrentControlSet\Control\Lsa\').LmCompatibilityLevel
    if ($reslmcl -eq $null){
        $neg_str + "LM Compatability Level registry key is not configured." | Tee-Object -FilePath $out_file -Append
    } else {
        if ([int]$reslmcl -eq 5){
            $pos_str + "LM Compatability Level is configured correctly: $reslmcl" | Tee-Object -FilePath $out_file -Append
        } else {        
            $neg_str + "LM Compatability Level is not configured to prevent LM and NTLM: $reslmcl" | Tee-Object -FilePath $out_file -Append
        }   
    }
}
Catch{
    $err_str + "Testing for LM Compatability registry key failed." | Tee-Object -FilePath $out_file -Append
}

# Restrict Anonymous Enumeration
$inf_str + "Testing Domain and Local Anonymous Enumeration settings: RestrictAnonymous." | Tee-Object -FilePath $out_file -Append
Try{
    $resra = (Get-ItemProperty -path 'HKLM:\System\CurrentControlSet\Control\Lsa\').RestrictAnonymous 
    if ($resra -eq $null){
        $neg_str + "RestrictAnonymous registry key is not configured." | Tee-Object -FilePath $out_file -Append
    } else {
        if ($resra){
            $pos_str + "RestrictAnonymous registry key is configured: $resra" | Tee-Object -FilePath $out_file -Append
        } else {        
            $neg_str + "RestrictAnonymous registry key is not configured: $resra" | Tee-Object -FilePath $out_file -Append
        }   
    }
}
Catch{
    $err_str + "Testing for Anonymous Enumeration RestrictAnonymous failed." | Tee-Object -FilePath $out_file -Append
}

$inf_str + "Testing Domain and Local Anonymous Enumeration settings: RestrictAnonymoussam" | Tee-Object -FilePath $out_file -Append
Try{
    $resras = (Get-ItemProperty -path 'HKLM:\System\CurrentControlSet\Control\Lsa\').RestrictAnonymoussam 
    if ($resras -eq $null){
        $neg_str + "RestrictAnonymoussam registry key is not configured." | Tee-Object -FilePath $out_file -Append
    } else {
        if ($resras){
            $pos_str + "RestrictAnonymoussam registry key is configured: $resras" | Tee-Object -FilePath $out_file -Append
        } else {        
            $neg_str + "RestrictAnonymoussam registry key is not configured: $resras" | Tee-Object -FilePath $out_file -Append
        }   
    }
}
Catch{
    $err_str + "Testing for Anonymous Enumeration RestrictAnonymoussam failed." | Tee-Object -FilePath $out_file -Append
}

###############################
# Secure MS Office
###############################
# Disable Office Macros
# Disable Office OLE

# Not implemented at this time.

###############################
# Restrict RPC Clients
###############################
# Configure restrictions for unauthenticated RPC clients
<#
Resources:
    Restrict Unauthenticated RPC clients: https://getadmx.com/?Category=Windows_10_2016&Policy=Microsoft.Policies.RemoteProcedureCalls::RpcRestrictRemoteClients
    Restrict unauthenticated RPC clients.: https://www.stigviewer.com/stig/windows_7/2017-12-01/finding/V-14253
#>

$inf_str + "Testing Restrict RPC Clients settings." | Tee-Object -FilePath $out_file -Append
$resrpc = (Get-ItemProperty -path 'HKLM:\Software\Policies\Microsoft\Windows NT\Rpc\' -ErrorAction SilentlyContinue).RestrictRemoteClients 
Try{
    if ($resrpc){
        $pos_str + "RestrictRemoteClients registry key is configured: $resrpc" | Tee-Object -FilePath $out_file -Append
    } else {        
        $neg_str + "RestrictRemoteClients registry key is not configured: $resrpc" | Tee-Object -FilePath $out_file -Append
    }   
}
Catch{
    $err_str + "Testing Restrict RPC Clients settings failed." | Tee-Object -FilePath $out_file -Append
}

###############################
# NTLM Settings
###############################
# Configure NTLM session security
<#
Resource:
    Session security for NTLM SSP-based servers must be configured to require NTLMv2 session security and 128-bit encryption.: https://www.stigviewer.com/stig/windows_server_2016/2018-03-07/finding/V-73697
    The system is not configured to meet the minimum requirement for session security for NTLM SSP based clients.: https://www.stigviewer.com/stig/windows_7/2012-07-02/finding/V-3382
#>

$inf_str + "Testing NTLM Session Server Security settings." | Tee-Object -FilePath $out_file -Append
Try{
    $resntssec = (Get-ItemProperty -path 'HKLM:\System\CurrentControlSet\Control\Lsa\MSV1_0' -ErrorAction SilentlyContinue).NtlmMinServerSec
    if ([int]$resntssec -eq 537395200){
        $pos_str + "NTLM Session Server Security settings is configured to require NTLMv2 and 128-bit encryption: $resntssec" | Tee-Object -FilePath $out_file -Append
    } else {        
        $neg_str + "NTLM Session Server Security settings is not configured to require NTLMv2 and 128-bit encryption: $resntssec" | Tee-Object -FilePath $out_file -Append
    }   
}
Catch{
    $err_str + "Testing NTLM Session Server Security settings failed." | Tee-Object -FilePath $out_file -Append
}

$inf_str + "Testing NTLM Session Client Security settings." | Tee-Object -FilePath $out_file -Append
$resntcsec = (Get-ItemProperty -path 'HKLM:\System\CurrentControlSet\Control\Lsa\MSV1_0' -ErrorAction SilentlyContinue).NtlmMinClientSec
Try{
    if ([int]$resntcsec -eq 537395200){
        $pos_str + "NTLM Session Client Security settings is configured to require NTLMv2 and 128-bit encryption: $resntcsec" | Tee-Object -FilePath $out_file -Append
    } else {        
        $neg_str + "NTLM Session Client Security settings is not configured to require NTLMv2 and 128-bit encryption: $resntcsec" | Tee-Object -FilePath $out_file -Append
    }   
}
Catch{
    $err_str + "Testing NTLM Session Client Security settings failed." | Tee-Object -FilePath $out_file -Append
}

########## Windows Information ##############
$inf_str + "Completed Date/Time: $(get-date -format yyyyMMddTHHmmssffzz)" | Tee-Object -FilePath $out_file -Append

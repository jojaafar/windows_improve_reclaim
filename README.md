# Windows Improve Reclaim

.SYNOPSIS
===========================
Automation for Windows 10, Windows 2016 Server, Windows 2019 Server
Uninstalling unnecessary Windows Applications 
Removing telemetry data forwarding for Windows, nVidia, Office 2016
for creating a GDPR compliant environment
Hardening these Windows flavors

This script and the according tools have to place in the directory c:\ccc\ . This absolute path is used for security reasons.

.DESCRIPTION
===========================  
Windows Server 2016/2019 and Windows 10 comes reasonably secure “out of the box”. But it’s important to remember 
that while the server is reasonably secure, not every security control that is can be configured for 
Windows Server 2016 (and the more recently released Windows Server 2019) is enabled on the 
operating system when you deploy it using default settings.

This is a PowerShell script for automation of routine tasks done after fresh installations of 
Windows 10, Windows Server 2016 and Windows Server 2019.

This is by no means any complete set of all existing Windows tweaks and neither is it another "antispying" type of script. 
Its simply a setting which I like to use and which in my opinion make the system less obtrusive.

for Window 10, Windows 2016 and Windows 2019

The Script is devided in three sections (Initial, Main, Special) for functionality selection.
To select a function or feature use the default settings or deselect with a remark sign like #.
Example Default

    "DisableDiagTrack",                 #"EnableDiagTrack",

    ==>>

   #"DisableDiagTrack",                 #"EnableDiagTrack",

    or

    "EnableDiagTrack",                  #"DisableDiagTrack",

Tweaked Win10 Main Setup Script

Included DISA STIG hardening for Windows 10, Windows 2016 Server, Windows 2019 Server and Office 2016.

Also many improvements for data privacy are in place such as disabling telemetry of Windows and Office.

.VERSION Overview
------------------
- vPierre modification 0.53.x
- vPierre modification 0.52.x
- vPierre modification 0.51.x
- vPierre modification 0.50.x
- vPierre modification 0.49.x
- vPierre modification 0.48.x

.NOTES License
===========================
MIT License
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

.NOTES GDPR
===========================
BayLDA
------
This report contains information about our approach and current findings concerning the 
Microsoft Windows 10 Enterprise investigation
- https://www.lda.bayern.de/media/windows_10_report.pdf

Dutch Impact Assessment
-----------------------
The Dutch government has commissioned a general data protection impact assessment on the
processing of data about the use of the Microsoft Office software. The purpose of this DPIA is to
help the individual government organisations map and assess the data protection risks for data
subjects caused by this data processing, and to ensure adequate safeguards to prevent or at least
mitigate these risks. This report provides a snapshot of the current risks. As Microsoft will
provide more information, and more research can be done to inspect the diagnostic data, new
versions of this DPIA will be drafted.
The Office software is deployed on a large scale by different governmental organisations, such as
ministries, the judiciary, the police and the taxing authority. Approximately 300.000 government
employees work with the software on a daily basis, to send and receive e-mails, create
documents and spreadsheets and prepare visual presentations. Generally, these organisations
store the content they produce with the Office software in governmental data centres, on
premise. Since the Dutch government currently tests the use of the online SharePoint / OneDrive
cloud storage facilities, this DPIA also includes the data Microsoft processes about the use of
SharePoint to store and access documents.
Federal negotiations versus individual DPIAs
The Dutch government has a Microsoft Strategic Vendor Management office (SLM Rijk). This
office conducts the negotiations with Microsoft for the federal government, but the individual
organisations buy the licenses and determine the settings and scope of the processing by
Microsoft Corporation in the USA. Therefore this general DPIA can help the different
government organisations with the DPIAs they must conduct, but this document does not
replace the specific risk assessments the different government organisations must make. Only
the organisations themselves can assess the specific data protection risks, based on their specific
deployment, the level of confidentiality of their work and the types of personal data they
process.
Scope: diagnostic data, not functional data
This report addresses the data protection risks of the storing by Microsoft of data about the
individual use of the Office software, including the use of Connected Services. These metadata
(about the use of the services and software) are called ‘diagnostic data’ in this report. This
includes so called ‘telemetry data’.

- https://www.rijksoverheid.nl/binaries/rijksoverheid/documenten/rapporten/2018/11/07/data-protection-impact-assessment-op-microsoft-office/DPIA+Microsoft+Office+2016+and+365+-+20191105.pdf

Microsoft
---------
Beginning your General Data Protection Regulation (GDPR) journey for Windows 10
- https://docs.microsoft.com/en-us/windows/privacy/gdpr-win10-whitepaper

Dutch Government
----------------
Dutch Government introduces GDPR Implementation Bill
- https://iapp.org/news/a/dutch-government-introduces-gdpr-implementation-bill/

.ROLE
===========================
Administrative rights are needed.

.OUTPUTS
===========================

Documentation and Log files
---------------------------

- $LogDir = "c:\log"
- $Log = "c:\log\log.txt"
- $DocuDir = "c:\docu\"
- $Docu = "c:\docu\docu.txt"

Power Logs
----------

- powercfg /systemsleepdiagnostics /output $DocuDir"system-sleep-diagnostics.html"
- powercfg /systemsleepdiagnostics /output $DocuDir"system-sleep-diagnostics.xml" /XML
- powercfg /systempowerreport /output $DocuDir"sleepstudy.html"
- powercfg /systempowerreport /output $DocuDir"sleepstudy.xml" /XML

DISM Log
--------

The DISM log file can be found at

- %systemroot%\Logs\DISM\dism.log

Firewall Log
------------

The firewall log file will be here

- %systemroot%\system32\LogFiles\Firewall\pfirewall.log

.OUTPUTS optional
-----------------
- c:\docu\winaudit.rtf
- c:\docu\winaudit.csv
- c:\docu\winaudit.html


.CONTRIBUTION GUIDELINES
===========================
Following is a list of rules which I'm trying to apply in this project. The rules are not binding and I accept pull 
requests even if they don't adhere to them, as long as their purpose and content are clear. In rare cases, 
when there are too many rule violations, I might simply redo the whole functionality and reject the PR 
(while still crediting you). If you'd like to make my work easier, please consider adhering to the following rules too.

Function naming
---------------
Try to give a function a meaningful name up to 25 characters long, which gives away the purpose of the function. 
Use verbs like Enable/Disable, Show/Hide, Install/Uninstall, Add/Remove in the beginning of the function name. 
In case the function doesn't fit any of these verbs, come up with another name, beginning with the 
verb Set, which indicates what the function does, eg. SetCurrentNetworkPrivate and SetCurrentNetworkPublic.

Revert functions
----------------
Always add a function with opposite name (or equivalent) which reverts the behaviour to default. The default is considered freshly installed 
Windows 10 or Windows Server 2016 or Windows Server 2019 with no adjustments made during or after the installation. If you don't have 
access to either of these, create the revert function to the best of your knowledge and I will fill in the rest if necessary.

Function similarities
---------------------
Check if there isn't already a function with similar purpose as the one you are trying to add. As long as the name and objective of the 
existing function is unchanged, feel free to add your tweak to that function rather than creating a new one.

Function grouping
-----------------
Try to group functions thematically. There are already five major groups (privacy, services, user interface, applications and 
server-specific), but even within these, some tweaks may be related to each other. In such case, add a new tweak below the 
existing one and not to the end of the whole group.

Default preset
--------------
Always add a reference to the tweak and its revert function in the $tweaks array containing the default set of tweaks. Add references to 
both functions on the same line (mind the quotes and commas) and always comment out the revert function. Whether to comment out also the 
tweak in the default preset is a matter of personal preference. The rule of thumb is that if the tweak makes the system faster, smoother, 
more secure and less obtrusive, it should be enabled by default. Usability has preference over performance (thats why e.g. indexing is 
kept enabled). Also don't forget to add the function references to all test files.

Repeatability
-------------
Unless applied on unsupported system, all functions have to be applicable repeatedly without any errors. When you're creating a registry key, 
always check first if the key doesn't happen to already exist. When you're deleting registry value, always append 
-ErrorAction SilentlyContinue to prevent errors while deleting already deleted values.

Input / output hiding
---------------------
Suppress all output generated by commands and cmdlets using | Out-Null or -ErrorAction SilentlyContinue where applicable. Whenever an input is needed, 
use appropriate parameters to suppress the prompt and programmatically provide values for the command to run (eg. using -Confirm:$false). 
The only acceptable output is from the Write-Output cmdlets in the beginning of each function and from non-suppressible cmdlets like 
Remove-AppxPackage -ErrorAction SilentlyContinue.

Registry
--------
Create the registry keys only if they don't exist on fresh installation if Windows 10 or Windows Server 2016. When deleting registry, 
delete only registry values, not the whole keys. When you're setting registry values, always use Set-ItemProperty instead of New-ItemProperty. 
When you're removing registry values, choose either Set-ItemProperty or Remove-ItemProperty to reinstate the same situation as it was on the 
clean installation. Again, if you don't know what the original state was, let me know in PR description and I will fill in the gaps. 
When you need to use HKEY_USERS registry hive, always add following snippet before the registry modification to ensure portability.

If (!(Test-Path "HKU:")) {
    New-PSDrive -Name HKU -PSProvider Registry -Root HKEY_USERS | Out-Null
}

Force usage
-----------
Star Wars jokes aside, don't use -Force parameter unless absolutely necessary. The only permitted case is when you're creating a 
new registry key (not a value) and you need to ensure that all parent keys will be created as well. In such case always check first 
if the key doesn't already exist, otherwise you will delete all its existing values.

Comments
--------
Always add a simple comment above the function briefly describing what the function does, especially if it has an ambiguous name or if there is 
some logic hidden under the hood. If you know that the tweak doesn't work on some editions of Windows 10 or on Windows Server, state it in the 
comment too. Add a Write-Output cmdlet with the short description of action also to the first line of the function body, so the user can see 
what is being executed and which function is the problematic one whenever an error occurs. The comment is written in present simple tense, 
the Write-Output in present continuous with ellipsis (resp. three dots) at the end.

Coding style
===========================

Indent using tabs, enclose all string values in double quotes (") and strictly use PascalCase wherever possible. Put opening curly bracket on the 
same line as the function name or condition, but leave the closing bracket on a separate line for readability.

COMMENT-BASED HELP KEYWORDS
---------------------------

The following are valid comment-based help keywords. They are listed in the order in which they typically appear in a help topic along with 
their intended use. These keywords can appear in any order in the comment-based help, and they are not case-sensitive.

.SYNOPSIS
---------
A brief description of the function or script. This keyword can be used only once in each topic.

.DESCRIPTION
------------
A detailed description of the function or script. This keyword can be used only once in each topic.

.PARAMETER
----------
The description of a parameter. Add a ".PARAMETER" keyword for each parameter in the function or script syntax.

Type the parameter name on the same line as the ".PARAMETER" keyword. Type the parameter description on the lines following the ".PARAMETER" 
keyword. Windows PowerShell interprets all text between the ".PARAMETER" line and the next keyword or the end of the comment block as part of the 
parameter description. The description can include paragraph breaks.

The Parameter keywords can appear in any order in the comment block, but the function or script syntax determines the order in which the parameters 
(and their descriptions) appear in help topic. To change the order, change the syntax.

You can also specify a parameter description by placing a comment in the function or script syntax immediately before the parameter variable name. 
If you use both a syntax comment and a Parameter keyword, the description associated with the Parameter keyword is used, and the syntax comment is ignored.

.EXAMPLE
--------
A sample command that uses the function or script, optionally followed by sample output and a description. Repeat this keyword for each example.

.INPUTS
-------
The Microsoft .NET Framework types of objects that can be piped to the function or script. You can also include a description of the input objects.

.OUTPUTS
--------
The .NET Framework type of the objects that the cmdlet returns. You can also include a description of the returned objects.

.NOTES
------
Additional information about the function or script.

.LINK
-----
The name of a related topic. The value appears on the line below the ".LINK" keyword and must be preceded by a comment symbol # or included in the comment block.

Repeat the ".LINK" keyword for each related topic.

This content appears in the Related Links section of the help topic.

The "Link" keyword content can also include a Uniform Resource Identifier (URI) to an online version of the same help topic. The online version opens 
when you use the Online parameter of Get-Help. The URI must begin with "http" or "https".

.COMPONENT
----------
The technology or feature that the function or script uses, or to which it is related. This content appears when the Get-Help command includes the
Component parameter of Get-Help.

.ROLE
-----
The user role for the help topic. This content appears when the Get-Help command includes the Role parameter of Get-Help.

.FUNCTIONALITY
--------------
The intended use of the function. This content appears when the Get-Help command includes the Functionality parameter of Get-Help.

.NOTES
===========================
Tested on 

- Windows 10
- Windows Server 2016
- Windows Server 2019 

up to Build Version 2004

.KNOWN ISSUES
===========================

- Get-Help
- Firewall rules (breaks existing rules; these rules are only valid for new machines)
- Install Fonts
- Improve documentation output